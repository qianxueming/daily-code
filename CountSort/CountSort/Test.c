#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

void Print(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void CountSort(int* a, int n)
{
	int max = a[0];
	int min = a[0];

	for (int i = 1; i < n; i++)//便利数据找到最大值和最小值确定数据范围
	{
		if (max < a[i])
		{
			max = a[i];
		}
		if (min > a[i])
		{
			min = a[i];
		}
	}

	int range = max - min + 1;//确定数据范围
	int* count = (int*)calloc(range, sizeof(int));//创建范围大小数组记录相同元素的个数
	if (count == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	for (int i = 0; i < n; i++)//在对应下标位置记录次数
	{
		count[a[i]-min]++;
	}

	int k = 0;
	for (int i = 0; i < range; i++)//将原数据覆盖
	{
		while (count[i]--)
		{
			a[k++] = i + min;
		}
	}
}

int main()
{
	int arr[] = { -3,-9,1,4,6,8,2,3,3,-1,-6 };
	CountSort(arr, sizeof(arr) / sizeof(arr[0]));
	Print(arr, sizeof(arr) / sizeof(arr[0]));
	return 0;
}