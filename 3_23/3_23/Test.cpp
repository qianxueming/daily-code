#include <iostream>
#include <stack>

using namespace std;

int main()
{
    while (1)
    {
        size_t num, n;
        cin >> num >> n;

        stack<char> st;

        if (num == 0)
            st.push('0');

        while (num)
        {
            size_t  data = num % n;
            num /= n;
            if (data < 10)
            {
                st.push('0' + data);
            }
            else
            {
                st.push('A' + data - 10);
            }
        }

        while (!st.empty())
        {
            cout << st.top();
            st.pop();
        }
        cout << endl;
   }
    return 0;
}