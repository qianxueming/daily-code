//#include <iostream>
//
//using namespace std;
//
//class Person
//{
//public:
//	void BuyTicket() { cout << "全票购买" << endl; }
//};
//
//class Student : public Person
//{
//public:
//	void BuyTicket(){ cout << "半价买票" << endl; }
//};
//
//void BuyTicket(Person* p)
//{
//	p->BuyTicket();
//}
//
//int main()
//{
//	Person p;
//	Student st;
//
//	Person* ptr1 = &p;
//	Person* ptr2 = &st;
//
//	BuyTicket(ptr1);//继承情况下，调用哪个函数是根据指针的类型来决定的 
//	BuyTicket(ptr2);
//
//
//	return 0;
//}

//多态的条件 -- 
// 1. 虚函数的重写,三同(返回值、函数名、参数) -- 多态之中之所以叫虚函数的重写是因为，多态是一种接口继承，也就是函数名、返回值、参数继承下来，重新定义实现
// 2. 使用父类的指针或者引用

//#include <iostream>
//
//using namespace std;
//
//class Person
//{
//public:
//	virtual void BuyTicket() { cout << "全票购买" << endl; }
//};
//
//class Student : public Person
//{
//public:
//	virtual void BuyTicket() { cout << "半价买票" << endl; }
//};
//
//void BuyTicket(Person* p)
//{
//	p->BuyTicket();
//}
//
//int main()
//{
//	Person p;
//	Student st;
//
//	Person* ptr1 = &p;
//	Person* ptr2 = &st;
//
//	BuyTicket(ptr1);// 满足多态后，调用哪个函数取决于指向的对象
//	BuyTicket(ptr2);
//
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//class Person
//{
//public:
//	~Person() { cout << "~Person()" << endl; }
//};
//
//class Student : public Person
//{
//public:
//	~Student() { cout << "~Student()" << endl; }
//};
//
//int main()
//{
//	Person* ptr1 = new Person;
//	Person* ptr2 = new Student;
//
//	delete ptr1;//由于是继承，因此调用函数是看指针类型的，因此调用的都是父类的析构函数
//	delete ptr2;
//
//	return 0;
//}
//
//#include <iostream>
//
//using namespace std;
//
//class Person
//{
//public:
//	virtual ~Person() { cout << "~Person()" << endl; }
//};
//
//class Student : public Person
//{
//public:
//	virtual ~Student() { cout << "~Student()" << endl; }
//};
//
//int main()
//{
//	Person* ptr1 = new Person;
//	Person* ptr2 = new Student;
//
//	delete ptr1;//满足多态后，调用的函数取决于指针指向的类型
//	delete ptr2;
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//class Person
//{
//public:
//	virtual void BuyTicket() { cout << "全票购买" << endl; }
//};
//
//class Student : public Person
//{
//public:
//	void BuyTicket() { cout << "半价买票" << endl; } //多态的特例，只要父类写了virtual，子类不写也能形成多态，但是建议在子类也加上virtual
//};
//
//void BuyTicket(Person* p)
//{
//	p->BuyTicket();
//}
//
//int main()
//{
//	Person p;
//	Student st;
//
//	Person* ptr1 = &p;
//	Person* ptr2 = &st;
//
//	BuyTicket(ptr1);// 满足多态后，调用哪个函数取决于指向的对象
//	BuyTicket(ptr2);
//
//	//ptr2->BuyTicket();//满足多态。调用的是子类的函数
//	//ptr2->Person::BuyTicket();//和继承类似，加访问限定符依旧可以调用父类的函数
//
//	return 0;
//}
//
//#include <iostream>
//
//using namespace std;
////多态的特例 -- 协变 -- 子类和父类的函数的返回值类型不同，但是也是父子类关系的指针或者引用（任意的父子关系都可以）
//
//class Person
//{
//public:
//	virtual Person* BuyTicket() 
//	{
//		cout << "全票购买" << endl; 
//		return this;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual Student* BuyTicket() 
//	{ 
//		cout << "半价买票" << endl; 
//		return this;
//	}
//};
//
//void BuyTicket(Person* p)
//{
//	p->BuyTicket();
//}
//
//int main()
//{
//	Person p;
//	Student st;
//
//	Person* ptr1 = &p;
//	Person* ptr2 = &st;
//
//	BuyTicket(ptr1);// 满足多态后，调用哪个函数取决于指向的对象
//	BuyTicket(ptr2);
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//class Person
//{
//public:
//	virtual void func(int val = 1) { cout << "Person" << " val = " << val << endl; }//满足多态的特例，三同，但是只有父类写了virtual
//	virtual void test() { func(); }//子类没有同名函数，virtual相当于没有写，但是在子类调用时，test成员函数的隐藏变量this是父类指针类型，因此满足多态的使用父类指针条件
//};
//
//class Student : public Person
//{
//public:
//	void func(int val = 0) { cout << "Student" << " val = " << val << endl; }//虚函数的重写--接口继承因此，val是1
//};
//
//int main()
//{
//	Student* st = new Student;
//
//	st->test();
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//class Person
//{
//public:
//	virtual void func(int val = 1) { cout << "Person" << " val = " << val << endl; }
//	virtual void test() { func(); }//同样因为test成员函数的隐藏变量this是父类指针类型，因此满足多态的使用父类指针条件
//};
//
//class Student : public Person
//{
//public:
//	void func(int val = 0) { cout << "Student" << " val = " << val << endl; }
//};
//
//int main()
//{
//	Student st;
//	st.test();
//
//	return 0;
//}


//#include <iostream>
//
//using namespace std;
//
//class Person
//{
//public:
//	virtual void func(int val = 1) { cout << "Person" << " val = " << val << endl; }
//};
//
//class Student : public Person
//{
//public:
//	void func(int val = 0) { cout << "Student" << " val = " << val << endl; }
//	virtual void test() { func(); }//由于不满足多态，也就是没有重写的概念了，因此val = 0
//};
//
//int main()
//{
//	Student* st = new Student;
//	st->test();
//
//	return 0;
//}

//继承强调的是继承函数实现
//多条强调的是继承函数接口，重写实现


//#include <iostream>
//
//using namespace std;
//
//class Person
//{
//public:
//	 virtual void func()final{};//C++11 final关键字让虚函数不能被重写
//};
//
//class Student : public Person
//{
//public:
//	virtual void func(){};
//};
//
//int main()
//{
//	Student* st = new Student;
//	st->func();
//
//	return 0;
//}


//#include <iostream>
//
//using namespace std;
//
//class Person
//{
//public:
//	 virtual void func(){};
//};
//
//class Student : public Person
//{
//public:
//	virtual void func()override{};//C++11 override关键字检查是否被重写
//};
//
//int main()
//{
//	Student* st = new Student;
//	st->func();
//
//	return 0;
//}

// 在虚函数后加 = 0，虚函数将变成纯虚函数，拥有纯虚函数的类叫做  抽象类  ，抽象类无法实例化对象
//class Car
//{
//public :
//	virtual void func() = 0
//	{
//
//	}
//};

//class Car
//{
//public:
//	virtual void func() = 0;//纯虚函数可以不写实现
//};
//
//int main()
//{
//	Car car;
//	return 0;
//}

//class Car
//{
//public:
//	virtual void func() = 0;//纯虚函数可以不写实现
//};
//
//class BMW: public Car//继承抽象类后如果不重写纯虚函数无法实例化对象
//{
//public:
//	
//};
//
//int main()
//{
//	BMW vmwcar;
//	return 0;
//}

//class Car
//{
//public:
//	virtual void func() = 0;
//};
//
//class BMW : public Car
//{
//public:
//	virtual void func()//重写纯虚函数，使得能够实例化对象
//	{}
//
//};
//
//int main()
//{
//	BMW vmwcar;
//	return 0;
//



//class Person
//{
//public:
//	virtual void func()//只要写了virtual就会进入虚函数表
//	{
//
//	}
//};
//
//int main()
//{
//	Person p;
//	return 0;
//}

#include <iostream>

using namespace std;

class Person
{
public:
	virtual void func()//只要写了virtual就会进入虚函数表
	{
		cout << "Person::func()" << endl;
	}
};

class Student : public Person
{
public:
	virtual void func()
	{
		cout << "Student::func()" << endl;
	}
};

int main()
{
	Person* p = new Student;
	p->func();
	return 0;
}

//实现多态的原理就是使用虚函数表，简称虚表
//拥有虚函数的对象会多一个隐藏的成员变量就是指向虚函数数组指针
//如果不满足多态，函数会直接调用不用去虚表寻找函数
//如果满足多态，才会去虚函数表寻找函数地址，虚表就和虚继承时使用的虚基表(表明偏移量)一样只有一份，这样所有的对象都可以使用，存在代码段
//虚函数的重写实际上就是在子类中将虚表的地址更换成子类的实现的函数的地址，因此在实现层面也叫做虚函数的覆盖
//多态需要使用父类的指针或者引用不能用对象的原因是，因为即使是将子类赋值给父类，父类也无法得到子类重写的虚表，如果父类对象通过赋值得到了子类重写虚表，那么就会造成混乱