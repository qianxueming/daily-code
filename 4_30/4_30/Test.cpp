///* ---- leetcode138
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    Node* next;
//    Node* random;
//
//    Node(int _val) {
//        val = _val;
//        next = NULL;
//        random = NULL;
//    }
//};
//*/
//
//class Solution {
//public:
//    Node* copyRandomList(Node* head) {
//        //测试默认输入[nullptr],value的值为nullptr
//
//        // map<Node*, Node*> m;
//        // m[nullptr];
//        // for (auto& e : m)
//        // {
//        //     cout << e.first << " " << e.second << endl;
//        // }
//        // return nullptr;
//
//
//        Node* newhead = nullptr, * newtail = nullptr;
//        Node* cur = head;
//
//        map<Node*, Node*> m;
//
//        while (cur)//复制链表
//        {
//            Node* newnode = new Node(cur->val);
//            if (newhead == nullptr)
//            {
//                newhead = newnode;
//                newtail = newnode;
//            }
//            else
//            {
//                newtail->next = newnode;
//                newtail = newtail->next;
//            }
//            m.insert(make_pair(cur, newtail));
//            cur = cur->next;
//        }
//        //m.insert(make_pair(nullptr, nullptr));
//
//
//        cur = head;
//        while (cur)
//        {
//            m[cur]->random = m[cur->random];//由于[]具有插入的功能因此，如果cur->random为空会自己插入一个<nullptr,nullptr>
//            //value是默认生成的nulptr值
//            cur = cur->next;
//        }
//
//        return newhead;
//    }
//};