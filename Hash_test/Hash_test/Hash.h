#pragma once

#include <vector>
#include <iostream>

using namespace std;


namespace OpenAddress
{

	enum State
	{
		EMPTY,
		EXIST,
		DELETE
	};

	template<class K, class V>
	struct HashData
	{
		pair<K, V> _kv;
		enum State _state = EMPTY;
	};

	template<class K, class V>
	class HashTable//闭散列
	{
	public:
		bool Insert(const pair<K, V>& kv)
		{
			if (Find(kv.first))
			{
				return false;
			}

			if (_tables.size() == 0 || _n * 10 / _tables.size() >= 7)//负载因子大于等于0.7扩容
			{
				size_t newsize = _tables.size() == 0 ? 10 : _tables.size() * 2;

				HashTable<K, V> newht;
				newht._tables.resize(newsize);//由于是在类内实例化的对象的因此可以访问私有成员
				for (auto& data : _tables)
				{
					if (data._state == EXIST)
					{
						newht.Insert(data._kv);
					}
				}

				_tables.swap(newht._tables);
			}

			//线性探测
			size_t hashi = kv.first % _tables.size();//不能使用capacity避免越界报错
			size_t index = hashi;
			size_t i = 1;

			while (_tables[index]._state == EXIST)
			{
				index = hashi + i;
				index = index % _tables.size();
				++i;
			}

			_tables[index]._kv = kv;
			_tables[index]._state = EXIST;
			_n++;

			return true;
		}

		HashData<K, V>* Find(const K& key)
		{
			if (_tables.size() == 0)
			{
				return nullptr;
			}

			size_t hashi = key % _tables.size();
			size_t index = hashi;
			size_t i = 1;
			while (_tables[index]._state != EMPTY)
			{
				if (_tables[index]._state == EXIST
					&& _tables[index]._kv.first == key)
				{
					return &_tables[index];
				}
				index = hashi + i;
				index %= _tables.size();
				++i;

				if (index == hashi)//下个循环搜索的位置是最开始的位置 -- 找一圈没找到,并且全是存在+删除，继续下去就是死循环 
				{
					break;
				}
			}

			return nullptr;
		}

		bool Erase(const K& key)
		{
			HashData<K, V>* ret = Find(key);
			if (ret != nullptr)
			{
				ret->_state = DELETE;
				--_n;
				return true;
			}
			else
			{
				return false;
			}
		}

	private:
		vector<HashData<K, V>> _tables;
		int _n = 0;//记录哈希表内存储的数据个数
	};

	void TestHashTable1()
	{
		int a[] = { 3, 33, 2, 13, 5, 12, 1002 };
		HashTable<int, int> ht;
		for (auto e : a)
		{
			ht.Insert(make_pair(e, e));
		}

		ht.Insert(make_pair(15, 15));

		ht.Erase(33);

		if (ht.Find(33))
		{
			cout << "33在" << endl;
		}
		else
		{
			cout << "33不在" << endl;
		}

	}

}

template<class K>
struct HashFunc
{
	size_t operator()(const K& key)
	{
		return key;
	}
};

template<>//模板的特化--兼容string类型(BKDRHash)
struct HashFunc<string>
{
	size_t operator()(const string& s)
	{
		size_t hash = 0;
		for (auto ch : s)
		{
			hash += ch;
			hash *= 31;
		}
		return hash;
	}
};

namespace HashBucket
{
	template<class K, class V>
	struct HashNode
	{
		struct HashNode<K, V>* _next;
		pair<K, V> _kv;

		HashNode(const pair<K, V>& kv)
			:_next(nullptr)
			,_kv(kv)
		{}
	};

	template<class K, class V, class Hash = HashFunc<K>>
	class HashTable
	{
	public:
		typedef HashNode<K, V> Node;

		~HashTable()
		{
			for (auto& cur : _tables)
			{
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				cur = nullptr;
			}
		}
	public:

		Node* Find(const K& key)
		{
			Hash hash;

			if (_tables.size() == 0)
			{
				return nullptr;
			}
			size_t hashi = hash(key) % _tables.size();
			Node* cur = _tables[hashi];
			while (cur)
			{
				if (key == cur->_kv.first)
				{
					return cur;
				}
				else
				{
					cur = cur->_next;
				}
			}
			return nullptr;
		}

		bool Erase(const K& key)
		{
			Hash hash;

			size_t hashi = hash(key) % _tables.size();
			Node* cur = _tables[hashi];
			Node* prev = nullptr;

			while (cur)
			{
				if (key == cur->_kv.first)
				{
					if (prev == nullptr)//头结点
					{
						_tables[hashi] = cur->_next;
					}
					else
					{
						prev->_next = cur->_next;
					}
					delete cur;
					return true;
				}
				else
				{
					prev = cur;
					cur = cur->_next;
				}
			}

			return false;
		}

		bool Insert(const pair<K, V>& kv)
		{
			Hash hash;

			if (_n == _tables.size())//负载因子等于1扩容
			{
				size_t newsize = _tables.size() == 0 ? 10 : _tables.size() * 2;

				vector<Node*> newtables(newsize, nullptr);
				//for (Node*& cur : _tables)
				for (auto& cur : _tables)
				{
					while (cur)
					{
						Node* next = cur->_next;

						size_t hashi = hash(cur->_kv.first) % newtables.size();
						//头插
						cur->_next = newtables[hashi];
						newtables[hashi] = cur;

						cur = next;
					}
				}

				_tables.swap(newtables);
			}

			size_t index = hash(kv.first) % _tables.size();

			Node* newnode = new Node(kv);
			newnode->_next = _tables[index];
			_tables[index] = newnode;
			_n++;

			return true;
		}

		size_t MaxBucketSize()
		{
			size_t max = 0;
			for (size_t i = 0; i < _tables.size(); ++i)
			{
				auto cur = _tables[i];
				size_t size = 0;
				while (cur)
				{
					++size;
					cur = cur->_next;
				}

				printf("[%d]->%d\n", i, size);//打印每个桶的长度
				if (size > max)
				{
					max = size;
				}
			}

			return max;//返回最长的桶的数量
		}
	private:
		vector<Node*> _tables;
		int _n = 0;
	};

	void TestHashTable1()
	{
		int a[] = { 3, 33, 2, 13, 5, 12, 1002 };
		HashTable<int, int> ht;
		for (auto e : a)
		{
			ht.Insert(make_pair(e, e));
		}

		ht.Insert(make_pair(15, 15));
	}
	void TestHashTable2()
	{
		HashTable<string, string> ht;

		ht.Insert(make_pair("sort", "排序"));
		ht.Insert(make_pair("left", "左边"));
		ht.Insert(make_pair("right", "右边"));
		ht.Insert(make_pair("test", "测试"));
	}

	void TestHashTable3()//经测试每个桶内数字个数为1~3占大多数，因此可以认为查找速度为O(N)(由于平衡因子限制条件满足后的扩容调整)
	{
		HashTable<int , int> ht;
		int N = 10000;
		srand(time(0));
		for (int i = 0; i < N; i++)
		{
			int key = rand() + i;
			ht.Insert(make_pair(key, key));
		}
		cout << ht.MaxBucketSize() << endl;
	}
}