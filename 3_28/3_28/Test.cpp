//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//    long long a = 0, b = 0;
//    cin >> a >> b;
//    if (a > b)//令a为较小值
//    {
//        swap(a, b);
//    }
//    long long num = b;
//    int flag = 0;
//    while (num < a * b)
//    {
//        if (num % a == 0 && num % b == 0)
//        {
//            cout << num << endl;
//            flag = 1;
//            break;
//        }
//        num++;
//    }
//    if (!flag)
//        cout << num << endl;
//    return 0;
//}


//#include <iostream>
//#include <string.h>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
////struct Less1
////{
////    bool operator()(string& s1, string& s2)
////    {
////        return s2.compare(s1);
////    }
////}myless1;
//
//struct Less1
//{
//    bool operator()(string& s1, string& s2)
//    {
//        return s1.compare(s2) < 0;
//    }
//}myless1;
//
//struct Less2
//{
//    bool operator()(string& s1, string& s2)
//    {
//        return s1.size() < s2.size();
//    }
//}myless2;
//
//int main()
//{
//    vector<string> v1;
//    int n = 0;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        string str;
//        cin >> str;
//        v1.push_back(str);//用小标访问为什么会出问题
//    }
//    vector<string> v2, v3;
//    v2 = v3 = v1;
//    sort(v2.begin(), v2.end(), myless1);
//    sort(v3.begin(), v3.end(), myless2);
//    int flag1 = 0;
//    for (int i = 0; i < n; i++)
//    {
//
//    }
//    int flag2 = 0;
//    for (int i = 0; i < n; i++)
//    {
//
//    }
//    if (flag1 + flag2 == 0)
//        cout << "none" << endl;
//    if (flag1 == 1 && flag2 == 0)
//        cout << "lexicographically" << endl;
//    if (flag1 == 0 && flag2 == 1)
//        cout << "lengths" << endl;
//    if (flag1 + flag2 == 2)
//        cout << "both" << endl;
//    return 0;
//}


#include <iostream>
#include <vector>

using namespace std;

int main()
{
	vector<int> v;
	v.reserve(5);//由于reserve函数
	//v.resize(5);
	v[0] = 1;//此操作是修改下标所指元素，而不是插入，由于调用reserve函数不改变vector的size因此会报越界错误
	cout << v[0] << endl;//error
	return 0;
}