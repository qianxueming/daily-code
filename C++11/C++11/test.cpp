#include <iostream>
#include <vector>
#include <list>
#include <initializer_list>
#include <map>
#include <algorithm>
#include <thread>
#include <cstdlib>
#include <windows.h>
using namespace std;

//struct point
//{
//	int _x;
//	int _y;
//};
//
//int main()
//{
//	int x1 = 1;
//	int x2 = { 2 };
//
//	//省略赋值符号--皆可用列表初始化
//	int x3{ 3 };
//	int arr1[]{ 1,2,3,4,5 };
//	int arr2[5]{ 0 };
//
//	// C++11中列表初始化也可以适用于new表达式中
//	int* pa = new int[4]{ 0 };
//
//	struct point p { 1, 2 };
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "Date(int year, int month, int day)" << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1(2022, 1, 1); // old style
//	// C++11支持的列表初始化，这里会调用构造函数初始化
//	Date d2{ 2022, 1, 2 };
//	Date d3 = { 2022, 1, 3 };
//	return 0;
//}

//int main()
//{
//	vector<int> v1 = { 1,2,3,4,5,6,7,8,9,0 };
//	
//	list<int> lt1 = { 9,8,7,6,5,4,3,2,1,0 };
//
//	auto i1 = { 1,2,3,4,5,6,7,8,9,0 };
//	auto i2 = { 9,8,7,6,5,4,3,2,1,0 };
//
//	cout << typeid(i1).name() << endl;
//	cout << typeid(i2).name() << endl;
//
//	initializer_list<int> l3 = { 1,2,3,4,5,6,7,8,9,0 };
//
//	vector<int>v3(l3.begin(), l3.end());
//
//	for (auto e : v3)
//	{
//		cout << e << ' ';
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	Date d1(2023, 5, 25);
//	Date d2(2023, 5, 26);
//
//	vector<Date> v1 = { d1,d2 };
//	vector<Date> v2 = { Date(2023,5,25),Date(2023,5,26) };
//	vector<Date> v3 = { {2023,5,25},{2023,5,26} };
//
//	map<string, string> m1 = { {"sort", "排序"},{"string","字符串"},{"count","计数"} };
//
//	pair<string, string> kv = { "sort","排序" };
//
//	return 0;
//}
//
//int main()
//{
//	const int x = 1;
//	double y = 2.2;
//
//	cout << typeid(x * y).name() << endl;
//
//	decltype(x * y) ret;
//	decltype(&x) p;
//
//	cout << typeid(ret).name() << endl;
//	cout << typeid(p).name() << endl;
//
//	//关键字decltype可以推导表达式类型
//	vector<decltype(x* y)> v;
//	cout << typeid(v).name() << endl;
//
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int b = 1;
//	int* p = &a;
//
//	//左值引用给左值起别名
//	int& ref1 = a;
//	int& ref2 = *p;
//
//	//左值引用给右值起别名
//	//int& ref3 = (a + b); -- a + b 返回的是临时变量具有常性
//	const int& ref3 = (a + b);
//	//ref3 = 6;
//	cout << ref3 << endl;
//	a = 3;
//	cout << ref3 << endl;
//
//	//右值引用给右值起别名
//	int&& ref4 = (a + b);
//
//	//右值引用给move的左值起别名
//	int&& ref5 = move(a);
//
//	return 0;
//}


//class myClass
//{
//public:
//	~myClass() = delete;
//private:
//	int a;
//};

//可变参数模板
//template<class ...Args>
//void ShowList(Args ...args)
//{
//	cout << sizeof...(args) << endl;
//}

//void ShowList()
//{
//	cout << endl;
//}
//
//template<class T, class ...Args>
//void ShowList(const T& val, Args ...args)
//{
//	cout << __FUNCTION__ << ":";
//	cout << val << " ";
//	ShowList(args...);//在参数包内参数个数为0时，调用无参的ShowList函数打印换行 - 递归函数展开参数包
//}
//
//template<class T>
//int PrintArray(T t)
//{
//	cout << t << " ";
//	return 0;
//}
//
//template<class ...Args>
//void ShowList(Args ...args)
//{
//	int arr[] = { PrintArray(args)... };
//	cout << endl;
//}
//
//int main()
//{
//	/*ShowList();
//	ShowList('a');
//	ShowList('a', 'b');
//	ShowList(66, 'a');*/
//	ShowList(66, 'a', string("hello"));
//
//	return 0;
//}


//lambda 表达式
//int main()
//{
//	auto add = [](int x, int y)->int { return x + y; };
//
//	cout << [](int x, int y)->int {return x + y; } (1, 2) << endl;
//	cout << add(1, 2) << endl;
//	return 0;
//}

//struct Goods
//{
//	string _name;  // 名字
//	double _price; // 价格
//	int _evaluate; // 评价
//
//	Goods(const char* str, double price, int evaluate)
//		:_name(str)
//		, _price(price)
//		, _evaluate(evaluate)
//	{}
//};
//
//int main()
//{
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2, 3 }, { "菠萝", 1.5, 4 } };
//
//	auto PriceLess = [](const Goods& g1, const Goods& g2)->bool {return g1._price < g2._price; };
//
//	sort(v.begin(), v.end(), PriceLess);
//
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2)->bool {
//		return g1._price > g2._price; 
//		});
//
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2)->bool {
//		return g1._evaluate < g2._evaluate; 
//		});
//
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2)->bool {
//		return g1._evaluate > g2._evaluate;
//		});
//
//
//
//	return 0;
//}


//int main()
//{
//	int x = 0, y = 1;
//	cout << x << ":" << y << endl << endl;
//
//	//传值
//	auto swap1 = [](int rx, int ry)
//	{
//		int temp = rx;
//		rx = ry;
//		ry = temp;
//	};
//	swap1(x, y);
//	cout << x << ":" << y << endl;
//
//
//	//传引用
//	auto swap2 = [](int& rx, int& ry)
//	{
//		int temp = rx;
//		rx = ry;
//		ry = temp;
//	};
//	swap2(x, y);
//	cout << x << ":" << y << endl;
//
//	auto swap3 = [x, y]() mutable//捕捉列表带常性，需加mutble,才能在lambda内修改,但也是传值捕捉
//	{
//		int temp = y;
//		x = y;
//		y = temp;
//	};
//	swap3();
//	cout << x << ":" << y << endl;
//
//	//引用捕捉
//	auto swap4 = [&x, &y]()
//	{
//		int temp = x;
//		x = y;
//		y = temp;
//	};
//	swap4();
//	cout << x << ":" << y << endl;
//	//全引用捕捉
//	auto swap5 = [&]()
//	{
//
//	};
//
//	//全传值捕捉
//	auto swap6 = [=]()
//	{
//
//	};
//	//除x外全引用捕捉
//	auto swap7 = [&, x]()
//	{
//
//	};
//	//除x外全传值捕捉
//	auto swap8 = [=, &x]()
//	{
//
//	};
//	return 0;
//}

//void Func(size_t n, int num)
//{
//	for (size_t i = 0; i < n; i++)
//	{
//		Sleep(1000);
//		cout << num << ":" << i << endl;
//	}
//}
//
//int main()
//{
//	thread t1(Func, 10, 1);
//	thread t2(Func, 15, 2);
//
//	t1.join();
//	t2.join();
//
//	return 0;
//}

//int main()
//{
//	int x = 0, y = 1;
//	int a = 2, b = 3;
//	thread t1([x, y]()
//		{
//			cout << x << ":" << y << endl;
//		});
//
//	thread t2([a, b]()
//		{
//			cout << a << ":" << b << endl;
//		});
//
//	t1.join();
//	t2.join();
//
//	return 0;
//}

//int main()
//{
//	int m;
//	cin >> m;
//	vector<thread> vtheds(m);//调用默认构造构造m个thread对象
//
//	for (int i = 0; i < m; i++)
//	{
//		int n;
//		cin >> n;
//		vtheds[i] = thread([i, n]() {//使用移动赋值
//			Sleep(10000);
//			for (int j = 0; j < n; j++)
//			{
//				Sleep(1000);
//				cout << this_thread::get_id() << ":" << j << endl;
//			}
//			});
//	}
//
//	for (auto& e : vtheds)//thread对象不允许生成拷贝构造，只能用引用
//	{
//		e.join();
//	}
//
//	return 0;
//}

#include <mutex>

//mutex mtx;
//int x = 0;
//
//void Func(int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		mtx.lock();
//		x++;
//		mtx.unlock();
//	}
//	/*mtx.lock();
//	for (int i = 0; i < n; i++)
//	{
//		x++;
//	}
//	mtx.unlock();*/
//}

int x = 0;
recursive_mutex mtx;//递归锁

void Func(int n)
{
	if (n == 0)
		return;

	mtx.lock();
	++x;
	Func(n - 1);
	mtx.unlock();
}

int main()
{
	int n = 1000;
	size_t begin = clock();

	thread t1(Func, n);
	thread t2(Func, 2 * n);

	t1.join();
	t2.join();

	size_t end = clock();

	cout << x << endl;
	cout << end - begin << endl;
	return 0;
}