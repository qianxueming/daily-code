#include <iostream>
#include <queue>
using namespace std;

int main()
{
	priority_queue<int> pq;
	pq.push(1);
	pq.push(3);
	pq.push(9);
	pq.push(2);
	pq.push(6);
	pq.push(4);
	pq.push(8);
	pq.push(7);
	pq.push(5);
	while (!pq.empty())
	{
		cout << pq.top() << " ";
		pq.pop();
	}
	cout << endl; //9 8 7 6 5 4 3 2 1
	return 0;
}