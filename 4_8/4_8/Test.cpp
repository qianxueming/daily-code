#include <iostream>
#include <string>

using namespace std;

//class Person
//{
//public:
//	void Print()
//	{
//		cout << "name = " << _name << " age = " << _age << endl;
//	}
//protected:
//	string _name = "name";//姓名
//	int _age = 18;//年龄
//};
//
//class Student : public Person
//{
//protected:
//	string stu;//学号
//};
//
//int main()
//{
//	Student st;
//	/*st.Print();*/
//
//	//只有公有继承才能将子类的赋值给父类/指针指向/引用
//	Person p = st;
//    Person* pi = &st;
//	Person& pp = st;
//	return 0;
//}


//class Person
//{
//protected:
//	string _name = "name";
//	int _num = 123;
//};
//
////基类和派生类有各自的作用域，基类和派生类有同名变量时，派生类优先使用派生类的变量，这叫做隐藏或者重定义
//class Student: public Person
//{
//public:
//	void Print()
//	{
//		cout << Person::_num << endl;
//		cout << _num << endl;
//	}
//protected:
//	int _num = 666;
//};
//
//
//int main()
//{
//	Student st;
//	st.Print();
//
//	return 0;
//}

//class Person
//{
//public:
//	void func()
//	{
//		cout << "Person::func()" << endl;
//	}
//};
//
////只要基类和派生类的函数名相同就构成隐藏，即使参数和返回值不同，最好不要将基类和子类的函数设计成同名，如果要调用基类函数还需要指定作用域
//class Student : public Person
//{
//public:
//	void func(int i)
//	{
//		cout << "func(int i)" << endl;
//	}
//};
//
//int main()
//{
//	Student st;
//	st.func(1);
//	//st.func(); -- 编译报错
//	st.Person::func();
//	return 0;
//}

//class Person
//{
//public:
//	Person(string name = "qxm", int age = 18)
//		:_name(name),
//		_age(age)
//	{
//		cout << "Person()" << endl;
//	}
//	Person(const Person& p)
//		:_name(p._name),
//		_age(p._age)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//	Person& operator=(const Person& p)
//	{
//		_name = p._name;
//		_age = p._age;
//		cout << "Person& operator=(const Person& p)" << endl;
//
//		return *this;
//	}
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//protected:
//	string _name = "qxm";
//	int _age = 18;
//};
//
//class Student : public Person
//{
//public:
//	Student(string name = "qxm_son", int stu = 18)//就像在自定义类型内定义了其他自定义类型类似，在派生类的构造函数会在初始化列表阶段调用基类的构造函数，即使在派生类的没有显示调用基类的构造函数
//	//基类或者派生类没有默认构造函数都会报错,只有调用基类的构造函数才能初始化基类继承的成员，因此最好在派生类的初始化列表显示调用基类的构造函数
//		:Person(name)
//		,_stu(stu)
//	{
//		cout << "Student()" << endl;
//	}
//	Student(const Student& st)//如果派生类中没有显示调用基类的拷贝构造函数，在拷贝构造生成派生类对象时，不会调用基类的拷贝构造函数
//		//而是会取调用基类的默认构造函数进行初始化
//		//因此需要显示调用基类的拷贝构造
//		//如果派生类不在派生类写拷贝构造函数，默认生成的拷贝构造会自动调用基类的拷贝构造
//		:Person(st)
//		,_stu(st._stu)
//	{
//
//	}
//	Student& operator=(const Student& st)//和拷贝构造函数相同，如果派生类中没有显示调用基类的赋值运算符重载，在子类进行赋值时，不会调用基类的赋值运算符重载
//		//而是会进行浅拷贝
//		//因此需要在基类的赋值运算符重载中显示调用基类的赋值运算符重载
//		//如果派生类不在派生写赋值重载，默认生成的赋值重载会自动调用基类的赋值重载
//	{
//		Person::operator=(st);
//		_stu = st._stu;
//
//		return *this;
//	}
//	~Student()
//	{
//		//~Person(); -- 编译报错，无法显示调用基类的析构函数，由于兼容多态，会导致析构函数被修饰成destructor
//		//Person::~Person();//--可以调用基类的析构函数，但是编译器也会调用一次，导致析构函数被调用两次，因此析构函数是没必要自己去显示调用的
//		cout << "~Student()" << endl;
//	}
//	//在派生类写默认成员函数的作用主要是为了满足需求，比如涉及深拷贝的问题
//protected:
//	int _stu;
//};
//
//int main()
//{
//	Student st;//对象实例化时，是先调用基类的构造后调用派生类的构造，而析构时是先调用派生类的析构，后调用基类的析构
//	//Student st1(st);
//	////Student st2("张三", 666);
//
//	////st2 = st1;
//	//Person p = st1;//调用了基类的拷贝构造
//	return 0;
//}


class A//由于析构函数私有，无法被继承，因为派生类的析构函数需要调用基类的析构函数
{
public:
	static const A& CreateObj()//写成静态是为了不创建对象调用，用这种方式可以在析构函数为私有时实例化对象
	{
		return A();
	}
private:
	~A()
	{}
};


int main()
{
	//A aa; -- 由于类的析构函数为私有，类外不能调用，因此不能实例化对象
	const A& aa = A::CreateObj();
	return 0;
}