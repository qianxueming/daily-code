#include "Queue.h"

void test01()
{
	Queue q;
	QueueInit(&q);

	QueuePush(&q, 1);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	QueuePush(&q, 4);
	QueuePush(&q, 5);

	printf("Front:%d\n", QueueFront(&q));
	printf("Back:%d\n", QueueBack(&q));
	printf("Size:%d\n", QueueSize(&q));
	
	QueuePop(&q);
	QueuePop(&q);
	QueuePop(&q);
	QueuePop(&q);
	QueuePop(&q);

	printf("Size:%d\n", QueueSize(&q));

	if (QueueEmpty(&q))
	{
		printf("empty\n");
	}
	else
	{
		printf("No empty\n");
	}

	QueueDestroy(&q);
}

int main()
{
	test01();
	return 0;
}