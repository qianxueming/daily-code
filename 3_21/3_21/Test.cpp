//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	string s("hello world");
//	
//	s.replace(5, 1, "%%d");
//
//	for (auto e : s)
//	{
//		cout << e;
//	}
//	return 0;
//}


//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	string s("cplusplus is the best language");
//
//	size_t pos = s.find(' ');
//
//	int num = 0;
//	for (auto e : s)//检查空格字符个数方便扩容
//	{
//		if (e == ' ')
//			num++;
//	}
//	s.reserve(s.size() + 2 * num);//提前扩容减少替换时扩容的次数
//
//	while (pos != string::npos)
//	{
//		s.replace(pos, 1, "***");
//		pos = s.find(' ', pos + 2);//每次都从上次找到并修改的位置后开始寻找
//	}
//	
//	for (auto e : s)
//	{
//		cout << e;
//	}
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	string s1("cplusplus is the best language");
//
//	int num = 0;
//	for (auto e : s1)//查找空格个数方便扩容
//	{
//		if (e == ' ')
//			num++;
//	}
//
//	string newstr;
//	newstr.reserve(s1.size() + 2 * num);//提前扩容，避免后续频繁扩容的消耗
//
//	for (auto e : s1)
//	{
//		if (e == ' ')
//			newstr += "***";
//		else
//			newstr += e;
//	}
//
//	for (auto e : newstr)
//	{
//		cout << e;
//	}
//
//	return 0;
//}


//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	string s("cplusplus is the best language");
//	
//	int pos = s.find(' ');
//
//	while (pos != string::npos)
//	{
//		s.replace(pos, 1, "***");
//		pos = s.find(' ', pos + 2);
//
//	}
//
//
//	for (auto e : s)
//	{
//		cout << e;
//	}
//	return 0;
//}


//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	string s("hello world");
//	
//	s += '\0';
//	s += '\0';
//	s += '\0';
//	s += "*****";
//
//	//string类重载了<<运算符
//	cout << s << endl;
//
//	cout << s.c_str() << endl;
//
//	return 0;
//}


//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	string s("hello world");
//
//	const string& newstr = s.substr(6);
//
//	cout << newstr << endl;
//	return 0;
//}


//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	string s("test.cpp.zip");
//
//	int pos = s.rfind('.');
//
//	string newstr = s.substr(pos);
//
//	cout << newstr << endl;
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	string s("test.cpp.zip");
//
//	int pos = s.rfind('.');
//
//	string::iterator it = s.begin() + pos;
//
//	while (it != s.end())
//	{
//		cout << *it;
//		it++;
//	}
//
//	return 0;
//}


//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	string s("cplusplus is the best language");
//
//	int pos = s.find_last_not_of("sseg");
//
//	cout << pos << endl;
//
//	cout << *(s.begin() + pos) << endl;
//
//	return 0;
//}
//
//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	string s1("hello wolrd");
//
//	string s2("i am you");
//
//	s1 == s2;
//	s1 == "hello wolrd";
//	"hellowolrd" == s1;
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	string s1("hello wolrd****************");
//	string s2("i am you************");
//	s1.swap(s2);
//
//	return 0;
//}

//#include <iostream>
//#include <string>
//
//using namespace std;
//
//int main()
//{
//	string s1;
//	string s2;
//	getline(cin, s1);
//	cin >> s2;
//
//	cout << s1 << endl;
//	cout << s2 << endl;
//	return 0;
//}


//#include <iostream>
//#include <string>
//
//using namespace std;
//
//int main()
//{
//    string s;
//    getline(cin, s);
//
//    string newstr;
//    newstr.reserve(s.size());
//
//    int start = s.rfind(' ');
//    int end = s.size();
//    while (start != string::npos)
//    {
//        newstr += s.substr(start + 1, end);
//        s[start] = '\0';
//        end = start;
//        start = s.rfind(' ', start - 1);
//    }
//    cout << newstr << endl;
//    return 0;
//}

//
//#include <iostream>
//#include <string>
//#include <vector>
//
//using namespace std;
//
//int main()
//{
//    string s;
//    getline(cin, s);
//    int num = 0;
//    for (auto e : s)
//    {
//        if (e == ' ')
//            num++;
//    }
//    vector<int> v1;
//    v1.reserve(num);
//    int pos = s.rfind(' ');
//    while (pos != string::npos)
//    {
//        v1.push_back(pos + 1);
//        s[pos] = '\0';
//        pos = s.rfind(' ', pos - 1);
//    }
//    for (int i = 0; i < v1.size(); i++)
//    {
//        int j = i;
//        while (s[j] != '\0')
//        {
//            cout << s[j];
//            j++;
//        }
//        cout << ' ';
//    }
//    cout << s.c_str() << endl;
//    return 0;
//}


//#include <iostream>
//#include <vector>
//
//using namespace std;
//
//int main()
//{
//    //取数
//    int n = 0;
//    cin >> n;
//    //特殊处理
//    if (n <= 2)
//    {
//        cout << 2;
//        return 0;
//    }
//    vector<int> v1;
//    v1.reserve(n);
//    int num = 0;
//    while (n--)
//    {
//        cin >> num;
//        v1.push_back(num);
//    }
//    int flag = 0;
//    for (int i = 1; i < v1.size() - 1; i++)
//    {
//        if (((v1[i - 1] <= v1[i]) && (v1[i] > v1[i + 1])) ||
//            ((v1[i - 1] >= v1[i]) && (v1[i] < v1[i + 1])))
//        {
//            flag++;
//            i += 2;
//        }
//    }
//    cout << (flag + 1);
//        return 0;
//}


//#include <iostream>
//#include <vector>
//
//using namespace std;
//
//int main()
//{
//    //取数
//    int n = 0;
//    cin >> n;
//    //特殊处理
//    if (n <= 2)
//    {
//        cout << 2;
//        return 0;
//    }
//    vector<int> v1;
//    v1.reserve(n);
//    int num = 0;
//    while (n--)
//    {
//        cin >> num;
//        v1.push_back(num);
//    }
//    vector<int>v2;
//    v2.reserve(v1.size());
//    for (int i = 0; i < v1.size(); i++)
//    {
//        if (i == 0)
//            v2.push_back(v1[i]);
//        else
//        {
//            if (v1[i] != v1[i - 1])
//                v2.push_back(v1[i]);
//        }
//    }
//    int flag = 0;
//    for (int i = 1; i < v2.size() - 1; i++)
//    {
//        if (((v2[i - 1] < v2[i]) && (v2[i] > v2[i + 1])) ||
//            ((v2[i - 1] > v2[i]) && (v2[i] < v2[i + 1])))
//        {
//            flag++;
//            i += 2;
//        }
//    }
//    cout << (flag + 1);
//        return 0;
//}



//#include<iostream>
//#include<vector>
//using namespace std;
//int main()
//{
//    int n;
//    cin >> n;
//    vector<int> a;
//    a.resize(n + 1);
//    for (auto& e : a)
//    {
//        cin >> e;
//    }
//    a[n] = 0;
//    int count = 0;
//    int i = 0;
//    while (i < n)//1 2 3 2 2 1
//    {
//        if (a[i] < a[i + 1])
//        {
//            while (a[i] < a[i + 1] && i < n)
//            {
//                i++;
//            }
//            count++;
//            i++;
//        }
//        if (a[i] == a[i + 1])
//        {
//            i++;
//        }
//        if (a[i] > a[i + 1])
//        {
//            while (a[i] > a[i + 1] && i < n)
//            {
//                i++;
//            }
//            count++;
//            i++;
//        }
//    }
//    cout << count;

//
//#include <iostream>
//#include <vector>
//
//using namespace std;
//
//class Solution {
//public:
//    int MoreThanHalfNum_Solution(vector<int>& numbers) {
//        //计数排序
//        int max = numbers[0];
//        int min = numbers[0];
//        for (auto e : numbers)
//        {
//            if (max < e)
//                max = e;
//            if (min > e)
//                min = e;
//        }
//
//        vector<int> v;
//        v.resize(max - min + 1);
//
//        for (auto e : numbers)
//        {
//            v[(e - min)]++;
//        }
//
//        int num = 0;
//        for (int i = 0; i < numbers.size(); i++)
//        {
//            if (v[i] > numbers.size() / 2)
//                num = i;
//        }
//        return num + min;
//    }
//};
//
//
//int main()
//{
//    vector<int> v;
//    v.push_back(1);
//    v.push_back(2);
//    v.push_back(3);
//    v.push_back(2);
//    v.push_back(2);
//    v.push_back(2);
//    v.push_back(5);
//    v.push_back(4);
//    v.push_back(2);
//    Solution().MoreThanHalfNum_Solution(v);
//
//    return 0;
//}

#include <iostream>
#include <string>

using namespace std;

class Solution {
public:
    bool isPalindrome(string s) {
        string newstr;
        newstr.reserve(s.size());
        for (auto e : s)
        {
            if (e >= 'a' && e <= 'z')
                newstr += e;
            if (e >= 'A' && e <= 'Z')
                newstr += (e + ('a' - 'A'));
        }
        int begin = 0;
        int end = newstr.size() - 1;
        while (begin < end)
        {
            if (newstr[begin] != newstr[end])
                return false;
            begin++;
            end--;
        }
        return true;
    }
};


int main()
{
    string s("0P");
    Solution().isPalindrome(s);

    return 0;
}
