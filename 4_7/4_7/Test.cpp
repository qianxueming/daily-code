#include <iostream>

using namespace std;

class Stock
{
public:
	Stock(char* _stockcode, int _quan = 1000, double _price = 8.98)
	{
		stockcode = _stockcode;
		quan = _quan;
		price = _price;
	}
	void Print()
	{
		cout << "stockcode is " << stockcode << " quan is " << quan << " price is " << price << endl;
	}
private:
	char* stockcode;
	int quan;
	double price;
};

int main()
{
	char na[128] = "600001";
	int q = 3000;
	double p = 5.67;
	Stock st1(na, q, p);
	st1.Print();
	Stock st2(na);
	st2.Print();
	return 0;
}