#pragma once

#include <vector>
#include <iostream>

using namespace std;

template<class K>
struct HashFunc
{
	size_t operator()(const K& key)
	{
		return key;
	}
};

template<>//模板的特化--兼容string类型(BKDRHash)
struct HashFunc<string>
{
	size_t operator()(const string& s)
	{
		size_t hash = 0;
		for (auto ch : s)
		{
			hash += ch;
			hash *= 31;
		}
		return hash;
	}
};

namespace HashBucket
{
	template<class T>
	struct HashNode
	{
		struct HashNode<T>* _next;
		T _data;

		HashNode(const T& data)
			:_next(nullptr)
			,_data(data)
		{}
	};

	template<class K, class T, class KeyOfT, class Hash>
	class HashTable;//提前声明--避免互相引用的问题

	template<class K, class T, class KeyOfT, class Hash>//KeyOfT解决map/set数据类型不同的比较问题(取key)，Hash解决数据类型不同的除留余数问题
	struct __HashIterator
	{
		typedef HashNode<T> Node;
		
		typedef HashTable<K, T, KeyOfT, Hash> HT;
		typedef __HashIterator<K, T, KeyOfT, Hash> Self;

		Node* _node;
		HT* _ht;

		__HashIterator(Node* node, HT* ht)
			:_node(node), _ht(ht)
		{}

		T& operator*()//*和->的使用对象不同因此返回值分为引用和指针
		{
			return _node->_data;
		}

		T* operator->()
		{
			return &_node->_data;
		}

		bool operator!=(const Self& it)
		{
			return _node != it._node;
		}

		//++it
		Self& operator++()
		{
			if (_node->_next != nullptr)
			{
				_node = _node->_next;
			}
			else
			{
				//寻找下一个非空的桶
				KeyOfT kot;
				Hash hash;
				size_t hashi = hash(kot(_node->_data)) % _ht->_tables.size();
				++hashi;

				while (hashi < _ht->_tables.size())
				{
					if (_ht->_tables[hashi])
					{
						_node = _ht->_tables[hashi];
						break;
					}
					else
					{
						++hashi;
					}
				}

				if (hashi == _ht->_tables.size())
				{
					_node = nullptr;
				}
			}
			return *this;
		}
			
	};

	template<class K, class T, class KeyOfT ,class Hash>
	class HashTable
	{
	public:
		typedef HashNode<T> Node;

		~HashTable()
		{
			for (auto& cur : _tables)
			{
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				cur = nullptr;
			}
		}
	public:
		template<class K, class T, class KeyOfT, class Hash>
		friend struct __HashIterator;

		typedef __HashIterator<K, T, KeyOfT, Hash> iterator;

		iterator begin()
		{
			Node* cur = nullptr;
			for (size_t i = 0; i < _tables.size(); i++)
			{
				if (_tables[i])
				{
					cur = _tables[i];
					break;
				}
			}

			return iterator(cur, this);
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}

		iterator Find(const K& key)
		{
			Hash hash;
			KeyOfT kot;

			if (_tables.size() == 0)
			{
				return end();
			}
			size_t hashi = hash(key) % _tables.size();
			Node* cur = _tables[hashi];
			while (cur)
			{
				if (key == kot(cur->_data))
				{
					return iterator(cur, this);
				}
				else
				{
					cur = cur->_next;
				}
			}
			return end();
		}

		bool Erase(const K& key)
		{
			Hash hash;
			KeyOfT kot;

			size_t hashi = hash(key) % _tables.size();
			Node* cur = _tables[hashi];
			Node* prev = nullptr;

			while (cur)
			{
				if (key == kot(cur->_data))
				{
					if (prev == nullptr)//头结点
					{
						_tables[hashi] = cur->_next;
					}
					else
					{
						prev->_next = cur->_next;
					}
					delete cur;
					return true;
				}
				else
				{
					prev = cur;
					cur = cur->_next;
				}
			}

			return false;
		}

		pair<iterator, bool> Insert(const T& data)
		{
			KeyOfT kot;
			iterator it = Find(kot(data));
			if (it != end())
			{
				return make_pair(it, false);
			}

			Hash hash;

			if (_n == _tables.size())//负载因子等于1扩容
			{
				size_t newsize = _tables.size() == 0 ? 10 : _tables.size() * 2;

				vector<Node*> newtables(newsize, nullptr);
				//for (Node*& cur : _tables)
				for (auto& cur : _tables)
				{
					while (cur)
					{
						Node* next = cur->_next;

						size_t hashi = hash(kot(data)) % newtables.size();
						//头插
						cur->_next = newtables[hashi];
						newtables[hashi] = cur;

						cur = next;
					}
				}

				_tables.swap(newtables);
			}

			size_t index = hash(kot(data)) % _tables.size();

			Node* newnode = new Node(data);
			newnode->_next = _tables[index];
			_tables[index] = newnode;
			_n++;

			iterator ret(newnode, this);
			return make_pair(ret, true);
		}

		size_t MaxBucketSize()
		{
			size_t max = 0;
			for (size_t i = 0; i < _tables.size(); ++i)
			{
				auto cur = _tables[i];
				size_t size = 0;
				while (cur)
				{
					++size;
					cur = cur->_next;
				}

				printf("[%d]->%d\n", i, size);//打印每个桶的长度
				if (size > max)
				{
					max = size;
				}
			}

			return max;//返回最长的桶的数量
		}
	private:
		vector<Node*> _tables;
		int _n = 0;
	};


}