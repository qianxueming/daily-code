#pragma once

#include "Hash.h"

namespace qxm
{
	template<class K, class Hash = HashFunc<K>>
	class unordered_set
	{
	public:
		struct SetKeyOfT
		{
			const K& operator()(const K& key)
			{
				return key;
			}
		};
	public:
		typedef typename HashBucket::HashTable<K, K, SetKeyOfT, Hash>::const_iterator iterator;//类模板的内嵌类型(类模板内含有的其他类对象)必须加typename,告知编译器先实例化类模板再实例化
		typedef typename HashBucket::HashTable<K, K, SetKeyOfT, Hash>::const_iterator const_iterator;

		pair<iterator, bool> insert(const K& key)
		{
			return _ht.Insert(key);
		}
	
		iterator begin()
		{
			return _ht.begin();
		}

		iterator end()
		{
			return _ht.end();
		}

		const_iterator begin()const
		{
			return _ht.begin();
		}

		const_iterator end()const
		{
			return _ht.end();
		}
		
		iterator find(const K& key)
		{
			return _ht.Find(key);
		}

		bool erase(const K& key)
		{
			return _ht.Erase(key);
		}

	private:
		HashBucket::HashTable<K, K, SetKeyOfT, Hash> _ht;
	};

	void print()
	{
		unordered_set<int> s;
		int a[] = { 3, 33, 2, 13, 5, 12, 1002, 33 };

		for (auto e : a)
		{
			s.insert(e);
		}
		cout << "begin" << endl;
		for (auto it : s)
		{
			it = 1;//---//识别成了整形，因此可以修改不影响unordered_set里面的值
			cout << it << ' ';
		}
		cout << "end" << endl;

		unordered_set<int>::iterator it = s.begin();
		while (it != s.end())
		{
			//*it = 1;//除了可以修改以外，由于除留余数法确定位置依照的是key，实现迭代器++使用了除留余数法，因此，key的修改可能造成死循环
			cout << *it << ' ';
			++it;
		}
		cout << endl;

		cout << endl;
	}

	void test_unordered_set()
	{
		unordered_set<int> s;
		int a[] = { 3, 33, 2, 13, 5, 12, 1002, 33};

		for (auto e : a)
		{
			s.insert(e);
		}

		unordered_set<int>::iterator it = s.begin();
		while (it != s.end())
		{
			//*it = 1;//除了可以修改以外，由于除留余数法确定位置依照的是key，实现迭代器++使用了除留余数法，因此，key的修改可能造成死循环
			cout << *it << ' ';
			++it;
		}
		cout << endl;

		for (auto it : s)
		{
			cout << it << ' ';
		}
		cout << endl;

		print();
	}
}
