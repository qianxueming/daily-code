#include "Sort.h"

void Swap(int* p1, int* p2)
{
	int temp = *p1;
	*p1 = *p2;
	*p2 = temp;
}

void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}


void InsertSort(int* a, int n)//插入排序
{
	for (int i = 0; i < n - 1; i++)
	{
		int end = i;
		int temp = a[end + 1];
		while (end >= 0)
		{
			if (temp < a[end])
			{
				a[end + 1] = a[end];
				--end;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = temp;
	}
}

void ShellSort(int* a, int n)//希尔排序
{
	int gap = n;
	while (gap > 1)
	{
		//gap /= 2;//无论奇数或者偶数最后都能变成1
		gap = gap / 3 + 1;//如果不加1不能保证最后是1
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int temp = a[end + gap];
			while (end >= 0)
			{
				if (temp < a[end])
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = temp;
		}
	}
}

//void SelectSort(int* a, int n)//选择排序
//{
//	
//	for (int i = 0; i < n - 1; i++)
//	{
//		int temp = i;
//		for (int j = i + 1; j < n; j++)
//		{
//			if (a[temp] > a[j])
//			{
//				temp = j;
//			}
//		}
//		Swap(&a[temp], &a[i]);
//	}
//	PrintArray(a, n);
//}

void SelectSort(int* a, int n)//选择排序
{
	int begin = 0, end = n - 1;
	while (begin < end)
	{
		int mini = begin, maxi = begin;
		for (int i = begin; i < n - begin; i++)
		{
			if (a[i] < a[mini])
			{
				mini = i;
			}
			if (a[i] > a[maxi])
			{
				maxi = i;
			}
		}
		Swap(&a[mini], &a[end]);
		if (maxi == end)
		{
			maxi = mini;
		}
		Swap(&a[maxi], &a[begin]);
		begin++;
		end--;
	}

	PrintArray(a, n);
}



void AdjustDown(int* a, int n, int parent)//向下调整
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		//确保指向的是大的那个孩子
		if (child + 1 < n && a[child + 1] > a[child])//大堆的调整
		{
			child++;
		}
		//如果父亲小于儿子，进行调整
		if (a[child] > a[parent])
		{
			Swap(&a[parent], &a[child]);
			parent = child;
			child = 2 * parent + 1;
		}
		//如果父亲大于儿子，调整结束
		else
		{
			break;
		}
	}
}

void HeapSort(int* a, int n)//堆排序
{

	//向下调整建堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(a, n, i);
	}
	for (int i = 0; i < n - 1; i++)
	{
		AdjustDown(a, n - i, 0);
		Swap(&a[0], &a[n - i - 1]);
	}

}

void BubbleSort(int* a, int n)//冒泡排序
{
	for (int i = 0; i < n; i++)
	{
		int exchange = 0;
		for (int j = 1; j < n - i; j++)
		{
			if (a[j] < a[j - 1])
			{
				Swap(&a[j], &a[j - 1]);
				exchange = 1;
			}
		}
		if (exchange == 0)
		{
			break;
		}
	}
}

int GetMidIndex(int* a, int begin, int end)
{
	int mid = (begin + end) / 2;
	if (a[begin] < a[mid])// begin mid
	{
		if (a[mid] < a[end])//   begin mid end
		{
			return mid;
		}
		else if (a[end] < a[begin])// end begin mid
		{
			return begin;
		}
		else
		{
			return end;
		}
	}
	else // a[begin] > a[mid]
	{
		if (a[end] < a[mid])// end mid begin 
		{
			return mid;
		}
		else if (a[end] > a[begin])//  mid begin end
		{
			return begin;
		}
		else
		{
			return end;
		}
	}
}

//void QuickSort(int* a, int begin, int end)//快速排序
//{
//	if (begin >= end)
//	{
//		return;
//	}
//
//	if ((end - begin + 1) < 15)//小区间优化
//	{
//		InsertSort(a + begin, (end - begin + 1));
//	}
//	else
//	{
//		int mid = GetMidIndex(a, begin, end);//三数取中
//		Swap(&a[begin], &a[mid]);
//
//		int left = begin, right = end;
//		int keyi = left;
//		while (left < right)
//		{
//			while (left < right && a[right] >= a[keyi])
//			{
//				right--;
//			}
//			while (left < right && a[left] <= a[keyi])
//			{
//				left++;
//			}
//			Swap(&a[left], &a[right]);
//		}
//		Swap(&a[left], &a[keyi]);
//		keyi = left;
//		QuickSort(a, begin, keyi - 1);
//		QuickSort(a, keyi + 1, end);
//	}
//}

//void QuickSort(int* a, int begin, int end)//快速排序
//{
//	if (begin >= end)
//	{
//		return;
//	}
//
//	if ((end - begin + 1) < 15)//小区间优化
//	{
//		InsertSort(a + begin, (end - begin + 1));
//	}
//	else
//	{
//	int mid = GetMidIndex(a, begin, end);//三数取中
//	Swap(&a[begin], &a[mid]);
//
//	int left = begin, right = end;
//	int hole = left;
//	int key = a[hole];
//	while (left < right)
//	{
//		//右边先走找小，填入坑位
//		while (left < right && a[right] >= key)
//		{
//			right--;
//		}
//		a[hole] = a[right];
//		hole = right;
//		//左边再走找大，填入坑位
//		while (left < right && a[left] <= key)
//		{
//			left++;
//		}
//		a[hole] = a[left];
//		hole = left;
//	}
//	a[hole] = key;
//	QuickSort(a, begin, hole - 1);
//	QuickSort(a, hole + 1, end);
//
//	}
//}

//void QuickSort(int* a, int begin, int end)//快速排序
//{
//	if (begin >= end)
//	{
//		return;
//	}
//
//	int keyi = begin;
//	int prev = begin, cur = begin + 1;
//	while (cur <= end)
//	{
//		//只有cur找到才进行交换，小的数往前挪，大的数往后挪
//		if (a[cur] < a[keyi] && ++prev != cur)
//			Swap(&a[cur], &a[prev]);
//		cur++;
//	}
//	Swap(&a[keyi], &a[prev]);
//	keyi = prev;
//	
//	QuickSort(a, begin, keyi - 1);
//	QuickSort(a, keyi + 1, end);
//}

int OnceQuickSort(int* a, int begin, int end)
{
	int keyi = begin;
	int prev = begin, cur = begin + 1;
	while (cur <= end)
	{
		//只有cur找到才进行交换，小的数往前挪，大的数往后挪
		if (a[cur] < a[keyi] && ++prev != cur)
			Swap(&a[cur], &a[prev]);
		cur++;
	}
	Swap(&a[keyi], &a[prev]);
	keyi = prev;
	return keyi;
}

#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

typedef int STDataType;

typedef struct Stack
{
	STDataType* a;
	int capacity;
	int top;
}ST;

bool StackEmpty(ST* ps)
{
	assert(ps);

	return ps->top == 0;
}

void StackInit(ST* ps)
{
	assert(ps);

	ps->a = (STDataType*)malloc(sizeof(STDataType) * 4);
	if (ps->a == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	ps->top = 0;
	ps->capacity = 4;
}

void StackDestroy(ST* ps)
{
	assert(ps);

	ps->a = NULL;
	ps->top = ps->capacity = 0;
}

void StackPush(ST* ps, STDataType x)
{
	assert(ps);

	if (ps->top == ps->capacity)
	{
		STDataType* tmp = (STDataType*)realloc(ps->a, sizeof(STDataType) * ps->capacity * 2);
		if (tmp == NULL)
		{
			perror("realloc fail");
			exit(-1);
		}

		ps->a = tmp;
		ps->capacity *= 2;
	}

	ps->a[ps->top] = x;
	ps->top++;
}

void StackPop(ST* ps)
{
	assert(ps);
	//assert(ps->top > 0);
	assert(!StackEmpty(ps));

	ps->top--;
}

STDataType StackTop(ST* ps)
{
	assert(ps);
	//assert(ps->top > 0);
	assert(!StackEmpty(ps));

	return ps->a[ps->top - 1];
}



void QuickSortNonR(int* a, int begin, int end)
{
	ST st;
	StackInit(&st);
	StackPush(&st, begin);
	StackPush(&st, end);

	while (!StackEmpty(&st))
	{
		int right = StackTop(&st);
		StackPop(&st);
		int left = StackTop(&st);
		StackPop(&st);

		int keyi = OnceQuickSort(a, left, right);
		// [left, keyi-1] keyi [keyi+1, right]
		if (keyi + 1 < right)
		{
			StackPush(&st, keyi + 1);
			StackPush(&st, right);
		}

		if (left < keyi - 1)
		{
			StackPush(&st, left);
			StackPush(&st, keyi - 1);
		}
	}

	StackDestroy(&st);
}
//
//void _MergeSort(int* a, int begin, int end, int* temp)//规定排序子函数
//{
//	if (begin >= end)//如果区间长度等于1认为有序
//	{
//		return;
//	}
//
//	int mid = (begin + end) / 2;//区间分割
//	int begin1 = begin, end1 = mid;
//	int begin2 = mid + 1, end2 = end;
//
//	_MergeSort(a, begin1, end1, temp);//对分割出的区间进行排序
//	_MergeSort(a, begin2, end2, temp);
//
//	int i = begin;
//	while (begin1 <= end1 && begin2 <= end2)//归并，即将数据按顺序插入
//	{
//		if (a[begin1] <= a[begin2])//保证稳定性
//		{
//			temp[i++] = a[begin1++];
//		}
//		else
//		{
//			temp[i++] = a[begin2++];
//		}
//	}
//
//	//如果一个区间内数据插入完，将另一个区间剩余的数据插入
//	while (begin1 <= end1)
//	{
//		temp[i++] = a[begin1++];
//	}
//
//	while (begin2 <= end2)
//	{
//		temp[i++] = a[begin2++];
//	}
//
//	memcpy(a + begin, temp + begin, sizeof(int)*(end - begin + 1));
//}
//
//void MergeSort(int* a, int n)//归并排序
//{
//	int* temp = (int*)malloc(sizeof(int) * n);
//	if (temp == NULL)
//	{
//		perror("malloc fail");
//		exit(-1);
//	}
//
//	_MergeSort(a, 0, n - 1, temp);
//
//}


void MergeSortNonR(int* a, int n)//归并排序的非递归实现
{
	int* temp = (int*)malloc(sizeof(int) * n);
	if (temp == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	int rangeN = 1;
	while (rangeN < n)
	{
		for (int i = 0; i < n; i += 2 * rangeN)
		{
			int begin1 = i, end1 = i + rangeN - 1;
			int begin2 = i + rangeN, end2 = i + 2 * rangeN - 1;

			//if (end1 >= n)
			//{
			//	end1 = n - 1;
			//	// 不存在区间
			//	begin2 = n;
			//	end2 = n - 1;
			//}
			//else if (begin2 >= n)
			//{
			//	// 不存在区间
			//	begin2 = n;
			//	end2 = n - 1;
			//}
			//else if (end2 >= n)
			//{
			//	end2 = n - 1;
			//}

			if (end1 >= n)
			{
				break;
			}
			else if (begin2 >= n)
			{
				break;
			}
			else if (end2 >= n)
			{
				end2 = n - 1;
			}

			//单趟归并
			int j = i;
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] <= a[begin2])//保证稳定性
				{
					temp[j++] = a[begin1++];
				}
				else
				{
					temp[j++] = a[begin2++];
				}
			}
			while (begin1 <= end1)
			{
				temp[j++] = a[begin1++];
			}
			while (begin2 <= end2)
			{
				temp[j++] = a[begin2++];
			}
			memcpy(a + i, temp + i, sizeof(int) * (end2 - i + 1));
		}
		rangeN *= 2;
	}
}
