
#include "Sort.h"

void TestInsertSort()
{
	int arr[] = { 9,5,4,6,7,3,2,1,6,8 };
	InsertSort(arr, sizeof(arr) / sizeof(arr[0]));
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));
}

void TestShellSort()
{
	//int arr[] = { 9,8,7,6,5,4,3,2,1 ,};
	int arr[] = { 9,8,7,6,5,4,3,2,1,6,8,7,3,4,1,2,9,0,-1,-6,-3};

	ShellSort(arr, sizeof(arr) / sizeof(arr[0]));
	//PrintArray(arr, sizeof(arr) / sizeof(arr[0]));

}

void TestSelectSort()
{
	//int arr[] = { 9,8,7,6,5,4,3,2,1};
	int arr[] = { 9,9,7,6,5,4,3,2,1,6,8,7,3,4,1,2,9,0,-1,-6,-3 };

	SelectSort(arr, sizeof(arr) / sizeof(arr[0]));

}

void TestHeapSort()
{
	int arr[] = { 9,8,7,6,5,4,3,2,1};
	HeapSort(arr, sizeof(arr) / sizeof(arr[0]));
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));
}

void TestBubbleSort()
{
	int arr[] = { 9,8,7,6,5,4,3,2,1 };
	BubbleSort(arr, sizeof(arr) / sizeof(arr[0]));
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));
}

void TestQuickSort()
{
	int arr[] = { 6,1,2,7,9,3,4,5,10,8 };
	QuickSortNonR(arr,0, sizeof(arr) / sizeof(arr[0])-1);
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));
}

void TestMergeSort()
{
	//int arr[] = { 6,1,2,7,9,3,4,5,10,8 };
	int arr[] = { 9,8,7,6,5,4,3,2,1 };

	MergeSortNonR(arr, sizeof(arr) / sizeof(arr[0]));
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));
}

//void TestOP()
//{
//	srand(time(0));
//	const int N = 1000000;
//	int* a1 = (int*)malloc(sizeof(int) * N);
//	int* a2 = (int*)malloc(sizeof(int) * N);
//	int* a3 = (int*)malloc(sizeof(int) * N);
//	int* a4 = (int*)malloc(sizeof(int) * N);
//	int* a5 = (int*)malloc(sizeof(int) * N);
//	int* a6 = (int*)malloc(sizeof(int) * N);
//	int* a7 = (int*)malloc(sizeof(int) * N);
//
//	int j = 0;
//	for (int i = 0; i < N; ++i)
//	{
//		a1[i] = rand();
//		/*int x = rand();
//		if (x % 7 == 0 && x % 3 == 0 && x % 2 == 0)
//		{
//		a1[i] = x;
//		++j;
//		}
//		else
//		{
//		a1[i] = i;
//		}*/
//
//		a2[i] = a1[i];
//		a3[i] = a1[i];
//		a4[i] = a1[i];
//		a5[i] = a1[i];
//		a6[i] = a1[i];
//		a7[i] = a1[i];
//	}
//	//printf("%d\n", j);
//
//	int begin1 = clock();
//	InsertSort(a1, N);
//	int end1 = clock();
//
//	int begin2 = clock();
//	ShellSort(a2, N);
//	int end2 = clock();
//
//	int begin3 = clock();
//	//SelectSort(a3, N);
//	int end3 = clock();
//
//	int begin4 = clock();
//	//HeapSort(a4, N);
//	int end4 = clock();
//
//	int begin7 = clock();
//	//BubbleSort(a7, N);
//	int end7 = clock();
//
//	int begin5 = clock();
//	//QuickSort(a5, 0, N - 1);
//	int end5 = clock();
//
//	int begin6 = clock();
//	//MergeSort(a6, N);
//	int end6 = clock();
//
//	printf("InsertSort:%d\n", end1 - begin1);
//	printf("ShellSort:%d\n", end2 - begin2);
//	//printf("SelectSort:%d\n", end3 - begin3);
//	//printf("HeapSort:%d\n", end4 - begin4);
//	//printf("BubbleSort:%d\n", end7 - begin7);
//
//	//printf("QuickSort:%d\n", end5 - begin5);
//	//printf("MergeSort:%d\n", end6 - begin6);
//
//	free(a1);
//	free(a2);
//	free(a3);
//	free(a4);
//	free(a5);
//	free(a6);
//}


int main()
{
	//TestInsertSort();
	//TestShellSort();
	//TestSelectSort();
	//TestHeapSort();
	//TestBubbleSort();
	//TestQuickSort();
	TestMergeSort();


	//TestOP();
	return 0;
}