#pragma once

#include <iostream>
#include <assert.h>

using namespace std;

class Date
{
	friend ostream& operator<<(ostream& out, const Date& d);

	friend istream& operator>>(istream& in, Date& d);

public:
	Date(int year = 1, int month = 1, int day = 1);

	void Print()const;

	//~Date() {}//析构函数

	//Date(const Date& d);//拷贝构造函数

	int GetMonthDay(int year, int month)const;

	Date GetAfterDay(int x)const;

	bool operator==(const Date& d)const;//==运算符重载

	bool operator!=(const Date& d)const;//!=运算符重载

	bool operator<(const Date& d)const;// < 运算符重载

	bool operator<=(const Date& d)const;// <= 运算符重载

	bool operator>(const Date& d)const;// > 运算符重载

	bool operator>=(const Date& d)const;// >= 运算符重载

	//Date& operator=(const Date& d);// = 运算符重载

	Date operator+(int x);//日期加天数

	Date& operator+=(int x);

	Date operator-(int x);

	Date& operator-=(int x);

	Date& operator++();//前置++

	Date operator++(int);//后置++

	Date& operator--();//前置--

	Date operator--(int);//后置--

	int operator-(const Date& d);

	void operator<<(ostream& out);

private:
	int _year;
	int _month;
	int _day;
};

//ostream& operator<<(ostream& out, const Date& d);
//
//istream& operator>>(istream& in, Date& d);

inline ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;

	return cout;
}

inline istream& operator>>(istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;

	return in;
}

