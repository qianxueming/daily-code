#include "Date.h"

Date::Date(int year, int month, int day)//构造函数
{
	assert(month > 0 && month < 13
		&& day > 0 && day <= GetMonthDay(year, month));
	_year = year;
	_month = month;
	_day = day;
	/*if (month > 0 && month < 13
		&& day > 0 && day <= GetMonthDay(year, month))
	{
		_year = year;
		_month = month;
		_day = day;
	}
	else
	{
		cout << "日期错误" << endl;
	}*/
}

void Date::Print()const
{
	cout << _year << "年" << _month << "月" << _day << "日" << endl;
}

//Date::Date(const Date& d)//拷贝构造函数
//{
//	_year = d._year;
//	_month = d._month;
//	_day = d._day;
//}

int Date::GetMonthDay(int year, int month)const//获得该月份天数
{
	assert(month > 0 && month < 13);

	int monthArray[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
	{
		return 29;
	}
	else
	{
		return monthArray[month];
	}
}


Date Date::GetAfterDay(int x)const//获得x天后的日期(不能传入负数)
{
	Date tmp = *this;//拷贝原数据

	tmp._day += x;//将x天加到日期上
	while (tmp._day > GetMonthDay(tmp._year, tmp._month))//如果加上x天后日期超过该月份最大天数，进行月进位
	{
		tmp._day -= GetMonthDay(tmp._year, tmp._month);//月份进位
		tmp._month++;

		if (tmp._month == 13)//月份超过最大月份，年份进位
		{
			tmp._year++;
			tmp._month = 1;
		}
	}

	return tmp;
}

bool Date::operator==(const Date& d)const//==运算符重载
{
	return _year == d._year
		&& _month == d._month
		&& _day == d._day;
}

bool Date::operator!=(const Date& d)const//!=运算符重载
{
	return !(*this == d);
}

bool Date::operator<(const Date& d)const// < 运算符重载
{
	if (_year < d._year)
	{
		return true;
	}
	else if (_year == d._year && _month < d._month)
	{
		return true;
	}
	else if (_year == d._year && _month && d._month && _day < d._day)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Date::operator<=(const Date& d)const// <= 运算符重载
{
	return *this < d || *this == d;
}

bool Date::operator>(const Date& d)const// > 运算符重载
{
	return !(*this <= d);
}

bool Date::operator>=(const Date& d)const// >= 运算符重载
{
	return !(*this < d);
}

//Date& Date::operator=(const Date& d)// = 运算符重载
//{
//	if (this != &d)
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//
//	return *this;
//}

Date& Date::operator+=(int x)
{
	if (x < 0)
	{
		*this -= -x;

		return *this;
	}

	_day += x;
	
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month == 13)
		{
			_year++;
			_month = 1;
		}
	}

	return *this;
}


Date Date::operator+(int x)//日期加天数c
{
	if (x < 0)
	{
		Date tmp = *this;
		tmp -= -x;
		return tmp;
	}

	Date tmp = *this;
	/*tmp._day += x;

	while (tmp._day > GetMonthDay(tmp._year, tmp._month))
	{
		tmp._day -= GetMonthDay(tmp._year, tmp._month);
		tmp._month++;
		if (tmp._month == 13)
		{
			tmp._year++;
			tmp._month = 1;
		}
	}*/
	tmp += x;

	return tmp;
}

Date& Date::operator++()//前置++
{
	*this += 1;

	return *this;
}

Date Date::operator++(int)//后置++
{
	Date tmp(*this);

	*this += 1;

	return tmp;
}

Date Date::operator-(int x)
{
	if (x < 0)
	{
		Date tmp = *this;
		tmp += -x;
		return tmp;
	}
	Date tmp(*this);

	tmp -= x;

	return tmp;
}

Date& Date::operator-=(int x)
{
	if (x < 0)
	{
		*this += -x;

		return *this;
	}
	_day -= x;

	while (_day <= 0)
	{
		_month--;

		if (_month == 0)
		{
			_year--;
			_month = 12;
		}
		
		_day += GetMonthDay(_year, _month);
	}

	return *this;
}

Date& Date::operator--()//前置--
{
	*this -= 1;

	return *this;
}

Date Date::operator--(int)//后置--
{
	Date tmp(*this);

	*this -= 1;

	return tmp;
}

int Date::operator-(const Date& d)
{
	Date max = *this;
	Date min = d;

	if (*this < d)
	{
		max = d;
		min = *this;
	}

	int n = 0;
	while (min != max)
	{
		++min;
		n++;
	}

	return n;
}

void Date::operator<<(ostream& out)
{
	out << _year << "年" << _month << "月" << _day << "日" << endl;
}

//
//ostream& operator<<(ostream& out, const Date& d)
//{
//	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;
//
//	return cout;
//}
//
//istream& operator>>(istream& in, Date& d)
//{
//	in >> d._year >> d._month >> d._day;
//
//	return in;
//}


