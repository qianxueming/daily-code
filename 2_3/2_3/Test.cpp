#include <iostream>

using namespace std;

class Date
{
public:
	//Date()//构造函数
	//{
	//	_year = 0;
	//	_month = 0;
	//	_day = 0;
	//}

	Date(int year = 1, int month = 1, int day = 1)//构造函数重载
	{
		_year = year;
		_month = month;
		_day = day;
	}

	void Print()
	{
		cout << _year << "年" << _month << "月" << _day << "日" << endl;
	}
private:
	int _year = 1;
	int _month = 1;
	int _day = 1;
};

class Stack
{
public:
	Stack(int n = 4)//构造函数
	{
		_a = (int*)malloc(sizeof(int) * n);
		if (_a == nullptr)
		{
			perror("malloc fail");
			exit(-1);
		}
		_size = 0;
		_capacity = n;
	}
	~Stack()//析构函数
	{
		cout << "~Stack()" << endl;
		free(_a);
		_a = nullptr;
		_size = 0;
		_capacity = 0;
	}
private:
	int* _a;
	int _size;
	int _capacity;
};

class Queue
{
public:

	//使用默认生成的构造函数
	~Queue()
	{
		cout << "~Queue" << endl;
		//delete[] popst;
	}

private:
	Stack popst[3];
	Stack pushst;
};

int main()
{/*
	Date d1;
	d1.Print();*/

	Queue q;

	return 0;
}
