#include <iostream>

using namespace std;

class Date
{
public:
	void Init(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	void func()
	{
		cout << "func()" << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date* ptr = nullptr;

	ptr->Init(2023, 2, 17);//运行崩溃
	ptr->func(); //正常运行
	(*ptr).func(); //正常运行
	return 0;
}

