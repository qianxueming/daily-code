#pragma once

#include <iostream>
#include <cstring>
#include <assert.h>

using namespace std;

namespace qxm
{
	class string
	{
	public:

		typedef char* iterator;

		typedef const char* const_iterator;

		string(const char* str = "");//无参构造函数

		string(const string& s);//拷贝构造

		~string();//析构函数

		string& operator=(const string& s);//赋值重载

		bool operator ==(const string& s);//相等重载

		bool operator !=(const string& s);//不相等重载

		bool operator >(const string& s);//大于重载

		bool operator >=(const string& s);//大于等于重载

		bool operator <(const string& s);//小于重载

		bool operator <=(const string& s);//小于等于重载

		char& operator[](size_t pos);//[]重载

		const char& operator[](size_t pos)const;//[]重载

		void reserve(size_t n);//扩容

		void resize(size_t n, char ch = '\0');//扩容

		void push_back(char ch);//尾插数据

		void append(const char* str);//尾插字符串

		string& operator+=(char ch);//+=重载

		string& operator+=(const char* ch);//+=重载

		void swap(string& s);//string对象交换函数

		void clear();//string数据清除函数

		const char* c_str()const;//以字符串数组方式返回

		size_t find(char ch, size_t pos = 0)const;//查找字符第一次出现的位置

		size_t find(char* ch, size_t pos = 0)const;//查找字符串第一次出现的位置

		string& insert(size_t pos, char ch);//在某一位置插入字符

		string& insert(size_t pos, const char* str);//在某一位置插入字符串

		string& erase(size_t pos, size_t len = npos);//删除某一位置后的n个数据

		size_t size()const//没有修改需求的变量传入时尽量加const修饰
		{
			return _size;
		}

		size_t capacity()const//没有修改需求的变量传入时尽量加const修饰
		{
			return _capacity;
		}

		bool empty()const
		{
			return _size == 0;
		}

		iterator begin()
		{
			return _str;
		}

        const_iterator begin()const
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		const_iterator end()const
		{
			return _str + _size;
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
		static const size_t npos;
	};
	void test_string01();
	void test_string02();
	ostream& operator<<(ostream& out, const qxm::string& s);//<<重载

	ostream& operator>>(ostream& in, const qxm::string& s);//>>重载
}




//ostream& operator<<(ostream& out, const qxm::string& s);//<<重载
//ostream& operator<<(ostream& out, const qxm::string& s)//<<重载
//{
//	for (auto e : s)
//	{
//		cout << e;
//	}
//	return out;
//}

