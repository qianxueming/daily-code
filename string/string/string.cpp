#include "string.h"

const size_t qxm::string::npos = -1;


qxm::string::string(const char* str)//如果不传入值就是用一个只有\0的空字符串进行初始化
	: _size(strlen(str))
{
	_capacity = _size;
	_str = new char[_capacity + 1];//多一位存储\0
	strcpy(_str, str);
	//_str[_size] = '\0';
}

qxm::string::string(const string& s)//拷贝构造
	:_size(s.size())
{
	_capacity = _size;
	_str = new char[_capacity + 1];
	strcpy(_str, s._str);
}

qxm::string::~string()//析构函数
{
	delete[] _str;
	_str = nullptr;
	_size = 0;
	_capacity = 0;
}

qxm::string& qxm::string::operator=(const string& s)//赋值重载
{
	if (this != &s)//赋值不仅能给别人赋值，还能给自身赋值如果是自身赋值下面的delete操作会清除原有数据
	{
		_size = s.size();
		_capacity = s.capacity();
		char* tmp = new char[_capacity + 1];//提前开空间，如果开空间失败会抛异常终止，下面的delete不会生效，不会影响原有数据
		delete[] _str;
		strcpy(tmp, s._str);
		_str = tmp;
	}

	return *this;
}

bool qxm::string::operator ==(const string& s)//相等重载
{
	return !strcmp(_str, s._str);
}

bool qxm::string::operator !=(const string& s)//不相等重载
{
	return !(*this == s);
}

bool qxm::string::operator >(const string& s)//大于重载
{
	if (strcmp(_str, s._str) > 0)
		return true;
	else
		return false;
}

bool qxm::string::operator >=(const string& s)//大于等于重载
{
	return *this > s || *this == s;
}

bool qxm::string::operator <(const string& s)//小于重载
{
	return !(*this > s) && !(*this == s);
}

bool qxm::string::operator <=(const string& s)//小于等于重载
{
	return *this < s || *this == s;
}

void qxm::string::reserve(size_t n)//扩容//******************************************************
{   //如果n<=capacity，reserve函数不做任何操作
	if (n > _capacity)
	{
		char* tmp = new char[n + 1];
		strcpy(tmp, _str);
		delete[] _str;
		_str = tmp;
		_capacity = n;
	}
}

void qxm::string::resize(size_t n, char ch)//扩容//***********************************************************
{
	if (n <= _size)
	{
		//取前n个值
		_str[n] = '\0';
		_size = n;
	}
	else
	{
		if (n > _capacity)//***************************************
		{
			reserve(n);
		}
		for (size_t i =_size; i < n; i++)
		{
			_str[i] = ch;
		}
		_size = n;
		_str[n] = '\0';
	}
}



void qxm::string::push_back(char ch)//尾插数据
{
	//if (_size < _capacity)//*********\0的位置可能出问题
	//{
	//	_str[_size] = c;
	//	_size++;
	//	//\0????
	//}
	//else
	//{
	//	size_t newcapacity = _capacity == 0 ? 3 : _capacity;//如果原string对象是空，2*capacity还是0
	//	reserve(2 * newcapacity);
	//	_str[_size] = c;
	//	_size++;
	//}
	if (_size + 1 > _capacity)//如果_size+1 == _capacity是插入后正好满数据的因为实现时申请的空间比_capacity多一个位置存储\0
	{
		size_t newcapacity = (_capacity == 0 ? 3 : 2 * _capacity);
		reserve(newcapacity);
	}
	_str[_size] = ch;
	_size++;

	_str[_size] = '\0';
}

void qxm::string::append(const char* str)//尾插字符串
{
	int len = strlen(str);
	if (_size + len > _capacity)//如果_size+1 == _capacity是插入后正好满数据的因为实现时申请的空间比_capacity多一个位置存储\0
	{
		size_t newcapacity = (_capacity == 0 ? len : _capacity + len);
		reserve(newcapacity);
	}

	strcpy(_str + _size, str);
	//strcat(_str, str);
	_size += len;
}

qxm::string& qxm::string::operator+=(char ch)//+=重载
{
	push_back(ch);
	return *this;
}

qxm::string& qxm::string::operator+=(const char* ch)//+=重载
{
	append(ch);
	return *this;
}

void qxm::string::swap(string& s)//string对象交换函数
{
	std::swap(_str, s._str);
	std::swap(_size, s._size);
	std::swap(_capacity, s._capacity);
}

void qxm::string::clear()//string数据清除函数
{
	if (_capacity > 0)
	{
		_str[0] = '\0';
		_size = 0;
	}
}

const char* qxm::string::c_str()const//以字符串数组方式返回
{
	return _str;
}


char& qxm::string::operator[](size_t pos)//[]重载
{
	assert(pos < _size);//索引不能大于等于，是越界访问。

	return _str[pos];
}

const char& qxm::string::operator[](size_t pos)const//[]重载
{
	assert(pos < _size);//索引不能大于等于，是越界访问。

	return _str[pos];
}

ostream& qxm::operator<<(ostream& out, const qxm::string& s)//<<重载
{
	for (auto e : s)
	{
		cout << e;
	}
	return out;
}

ostream& qxm::operator>>(ostream& in, const qxm::string& s)//>>重载
{
	char ch = 0;
	//while ()

	return in;
}

size_t qxm::string::find(char ch, size_t pos) const//查找字符第一次出现的位置
{
	assert(pos < _size);
	for (size_t i = pos; i < _size; i++)
	{
		if (_str[i] == ch)
			return i;
	}

	return qxm::string::npos;
}


size_t qxm::string::find(char* ch, size_t pos)const//查找字符串第一次出现的位置
{
	assert(pos < _size);

	// kmp
	char* p = strstr(_str + pos, ch);
	if (p == nullptr)
	{
		return npos;
	}
	else
	{
		return p - _str;
	}
	return qxm::string::npos;
}

qxm::string& qxm::string::insert(size_t pos, char ch)//在某一位置插入字符
{
	assert(pos >= 0 && pos <= _size);
	if (_size + 1 >= _capacity)
	{
		reserve(2 * _capacity);
	}

	size_t i = _size;
	while (i > pos)
	{
		_str[i] = _str[i - 1];
		i--;
	}
	_str[pos] = ch;
	_size++;
	return *this;
}

qxm::string& qxm::string::insert(size_t pos, const char* str)//在某一位置插入字符串
{
	assert(pos >= 0 && pos <= _size);

	size_t len = strlen(str);
	if (_size + len >= _capacity)
	{
		reserve(_size + len);
	}

	size_t i = _size + len - 1;
	while (i > pos + len - 1)
	{
		_str[i] = _str[i - len];
		i--;
	}
	_size += len;

	while (len > 0)
	{
		_str[--len] = str[len];
	}
	return *this;
}

qxm::string& qxm::string::erase(size_t pos, size_t len)//删除某一位置后的n个数据
{
	assert(pos < _size);

	if (len == npos || pos + len >= _size)
	{
		_str[pos] = '\0';
		_size = pos;
	}
	else
	{
		strcpy(_str + pos, _str + pos + len);
		_size -= len;
	}

	return *this;
}


void qxm::test_string01()
{
	//string s1("hello world");//传入的是字符串常量，即使是C语言定义字符数组等于这段字符串也是将这段常量字符串一个一个赋值过去的
}


void qxm::test_string02()
{
	
	//string s1("abc");
	////cout << s1.empty() << endl;
	////std::string s2("xxxxxxxxxxx");
	//string s2("xxxxxxxxxxx");
	////s2.reserve(3);
	//s2.append("hello world");
	//s2 += '5';
	////cout << s2.c_str() << endl;
	//(s2 += "6666666666") += "44444444";
	//std::string s3("xxxxxxxxxx");
	//s3[4] = '\0';
	//cout << s3 << endl;
	//cout << s2 << endl;
	//s2[4] = '\0';
	//cout << s2 << endl;

	string s("hello world");
	s.insert(0, "66666");
	for (auto e : s)
	{
		cout << e;
	}
	//cout << s;


	/*cout << s2.c_str() << endl;

	s1.swap(s2);

	string s3;
	s3.clear();

	cout << s2.c_str() << endl;*/

	/*s2.push_back('a');
	s2.push_back('a');
	s2.push_back('a');
	s2.push_back('a');
	s2.push_back('a');
	s2.push_back('a');
	s2.push_back('a');
	s2.push_back('a');*/

	/*for (auto e : s2)
	{
		cout << e;
	}
	*/

	/*s2.reserve(3);
	s2.reserve(20);*/
	/*s2.resize(3);
	s2.resize(30, 'z');*/

	

	//for (size_t i = 0; i < s2.size(); i++)
	//{
	//	s2[0]++;
	//	cout << s2[i] << " ";
	//}
	//string s3(s2);
	//s1 = s2;

	//cout << (s1 <= s2) << endl;

	/*qxm::string::iterator it = s1.begin();
	while (it != s1.end())
	{	
		cout << *it << ' ';
		it++;
	}*/
	/*qxm::string::const_iterator it = s2.begin();
	while (it != s2.end())
	{
		cout << *it << ' ';
		it++;
	}*/
}