//#include <iostream>
//
//using namespace std;
//
//class Person
//{
//public:
//	void func() { cout << "Person" << endl; }
//};
//
//class Student : public Person
//{
//public:
//	virtual void func(){ cout << "Student" << endl; }
//};
//
//int main()
//{
//
//	Student().func();
//	Student().Person::func();//如果不构成虚表的覆盖，函数的调用在使用方面与继承时无异，类似于隐藏
//	return 0;
//}



//#include <iostream>
//
//using namespace std;
//
//typedef void (*virtual_func) ();
//
//class Person
//{
//public:
//	void func() { cout << "Person" << endl; }
//};
//
//class Student : public Person
//{
//public:
//	virtual void func1(){ cout << "Student func1 " << endl; }
//	virtual void func2() { cout << "Student func2 " << endl; }
//	virtual void func3() { cout << "Student func3 " << endl; }
//
//};
////--打印虚函数表
//int main()
//{ 
// //拥有虚函数的对象，对象第一个存放的变量是虚函数表指针，也就是虚函数指针数组
//	Student st; 
//	int* ptr1= (int*)&st;
// //指针大小是四字节，因此先得到对象的地址强转成int*进行解引用得到的数据大小是四个字节，也就是对象前四个字节存放的数据，也就是虚表指针数组的首地址
//	int ptr2 = *ptr1;
//	virtual_func* ptr = (virtual_func*)ptr2;//得到虚表指针数组的首地址后，将其强转成虚表指针的指针
//
//	for ( ; *ptr; ptr++)//visual studio 下虚表会以空指针结尾，因此判断条件为虚表指针的值
//	{
//		cout << *ptr << endl;
//	}
//	return 0;
//}

//虚表是在编译时生成
//虚表会在初始化列表时初始化到对象中
//虚表存在常量区(代码段)

//#include <iostream>
//
//using namespace std;
//
//class Base1
//{
//public:
//};
//
//class Base2
//{
//public:
//};
//
//class Base3 : public Base1 , public Base2
//{
//public:
//	virtual void func() { cout << "func()" << endl; }
//};
//
//int main()
//{
//
//	Base3 b3;
//	Base2* b2 = &b3;
//	Base1 b1;
//	
//
//	return 0;
//}
