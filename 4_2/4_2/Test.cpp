#include <iostream>

using namespace std;

class Solution {
public:
    bool isPalindrome(string s) {
        string newstr;
        newstr.reserve(s.size());
        for (auto e : s)
        {
            if (e >= 'a' && e <= 'z')
                newstr += e;
            if (e >= 'A' && e <= 'Z')
                newstr += (e + ('a' - 'A'));
            if (e >= '0' && e <= '9')
                newstr += e;
        }
        int begin = 0;
        int end = newstr.size() - 1;
        while (begin < end)
        {
            if (newstr[begin] != newstr[end])
            {
                return false;
            }
            begin++;
            end--;
        }
        return true;
    }
};