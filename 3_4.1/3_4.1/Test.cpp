#include <iostream>

using namespace std;

class A
{
public:
	void Print()const
	{
		cout << _a << endl;
	}
	A* operator&()
	{
		return this;
	}
	const A* operator&()const
	{
		return this;
	}
private:
	int _a = 10;
};

int main()
{
	A aa;
	const A bb;

	cout << &aa << endl;
	cout << &bb << endl;

	return 0;
}