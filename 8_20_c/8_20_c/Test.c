#define  _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

int main()
{
	printf("hello world\n");
	goto flag;
	printf("hello goto\n");
flag:
	printf("hello blog\n");
	return 0;
}

//int main()
//{
//    int i = 0;
//    do
//    {
//        i = i + 1;
//        if (3 == i)
//            continue;
//        printf("%d ", i);
//    } while (i < 5);
//
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int i = 1;
//	do
//	{
//		printf("%d ", i);
//		i++;
//		
//		if (i == 2)
//		{
//			continue;
//		}
//	} while (i <= 5);
//	return 0;
//}

//int main()
//{
//    int i = 0;
//    int k = 0;
//    for (i = 0, k = 0; k = 0; i++, k++)
//        k++;
//    return 0;
//}
//int main()
//{
//    int x, y;
//    for (x = 0, y = 0; x < 2 && y < 5; ++x, y++)
//    {
//        printf("hehe\n");
//    }
//    return 0;
//}

//int main()
//{
//    int i = 0;
//    int j = 0;
//    for (; i < 10; i++)
//    {
//        for (; j < 10; j++)
//        {
//            printf("hehe\n");
//        }
//    }
//    return 0;
//}

//int main()
//{
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < 10; i++)
//    {
//        for (j = 0; j < 10; j++)
//        {
//            printf("hehe\n");
//        }
//    }
//}

//int main()
//{
//	for (;;)
//	{
//		printf("hehe\n");
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 1; i < 5; i++)
//	{
//		if (i == 3)
//			continue;
//		printf("%d ", i);
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 1; i < 10; i++)
//	{
//		printf("%d ", i);
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	while (i < 5)
//	{
//		i++;
//		if (i == 3)
//			continue;
//		printf("%d ", i);
//	}
//	return 0;
//}

//int main()
//{
//	int day = 0;
//	scanf("%d", &day);
//	switch (day)
//	{
//	case 1:
//		printf("星期一\n");
//		break;
//	case 2:
//		printf("星期二\n");
//		break;
//	case 3:
//		printf("星期三\n");
//		break;
//	case 4:
//		printf("星期四\n");
//		break;
//	case 5:
//		printf("星期五\n");
//		break;
//	case 6:
//		printf("星期六\n");
//		break;
//	default:
//		printf("输入错误\n");
//		/*break;*/
//	case 7:
//		printf("星期天\n");
//		break;
//	/*default:
//		printf("输入错误\n");
//		break;*/
//	}
//	return 0;
//}

//int main()
//{
//	int day = 0;
//	scanf("%d", &day);
//	switch (day)
//	{
//	case 1:
//		printf("星期一\n");
//	case 2:
//		printf("星期二\n");
//	case 3:
//		printf("星期三\n");
//	case 4:
//		printf("星期四\n");
//	case 5:
//		printf("星期五\n");
//	case 6:
//		printf("星期六\n");
//	case 7:
//		printf("星期天\n");
//	}
//	return 0;
//}

//int main()
//{
//    int a = 0;
//    int b = 2;
//    if (a == 1)
//    {
//        if (b == 2)
//            printf("hehe\n");
//        else
//            printf("haha\n");
//    }
//    return 0;
//}

//int main()
//{
//	int age = 59;
//	if (age < 18)
//		printf("未成年\n");
//	else if (age >= 18 && age < 60)
//		printf("成年\n");
//	else
//		printf("老年\n");
//	return 0;
//