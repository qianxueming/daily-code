//#include <iostream>
//#include <vector>
//
//using namespace std;
//
//
//class Solution {
//public:
//    int fib(int n) {
//        vector<int> v(n + 1);
//    }
//};
//
//int Get_MonthDay(int year, int month)
//{
//    vector<int> v = { 0, 0, 31 , 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };
//    if (((year % 4 == 0) && (year % 100 != 0)) ||
//        (year % 400 == 0))
//        return v[month] + 1;
//    else
//        return  v[month];
//}
//
//int main()
//{
//    int year, month, day;
//    cin >> year >> month >> day;
//    cout << (Get_MonthDay(year, month) + day) << endl;
//    return 0;
//}


#include <iostream>
#include <vector>

using namespace std;

int test_0_1bag(vector<int>& weight, vector<int>& value, int bag_contain)//0_1背包问题二维数组求解
{
	//dp[i][j] 表示从下标为[0-i]的物品里任意取，放进容量为j的背包，价值总和最大是多少
	//i表示第i个物品，j表示背包容量
	vector<vector<int>> dp(weight.size(), vector<int>(bag_contain + 1, 0));//dp数组初始化，初始化成bag_contain+1下标对应背包容量

	//由于dp[i][j]如果在二维数组中来看，是取决于左上元素和上方元素的大小，因此必须初始化第一行和第一列的数据方便后续求值
	for (int j = weight[0]; j <= bag_contain; j++) {
		dp[0][j] = value[0];
	}

	//dp[i][j] 如果物品i不放入背包，最大价值取决于dp[i-1][j]
	//如果dp[i][j]放入背包,最大价值取决于 dp[i-1][j-weight[j]] + value[i],
	//因此dp[i][j]最大价值取以上两者最大值
	for (int i = 1; i < dp.size(); i++)
	{
		for (int j = 0; j < dp[0].size(); j++)
		{
			if (j < weight[i]) dp[i][j] = dp[i - 1][j];
			else dp[i][j] = max<int>(dp[i - 1][j], (dp[i - 1][j - weight[i]] + value[i]));
		}
	}
	return dp[dp.size()-1][bag_contain];
}

int main()
{
	vector<int> weight = { 1, 3, 4 };
	vector<int> value = { 15, 20, 30 };
	int bag_contain = 4;
	int ret = test_0_1bag(weight, value, bag_contain);

	cout << ret << endl;

	return 0;
}