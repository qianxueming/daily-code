#include <iostream>

using namespace std;

class Solution {//字符串相加
public:
    string addStrings(string num1, string num2) {
        string s;
        int i = num1.size() - 1;
        int j = num2.size() - 1;
        int add = 0;
        while (i >= 0 && j >= 0)
        {
            if (num1[i] + num2[j] - '0' + add <= '9')//某一位相加小于10
            {
                s.insert(0, 1, (num1[i] + num2[j] + add - '0'));
                add = 0;
            }
            else//某一位相加大于等于10
            {
                s.insert(0, 1, (num1[i] + num2[j] + add - '0' - 10));
                add = 1;
            }
            i--;
            j--;
        }
        while (add == 1 && i >= 0)
        {
            if (num1[i] + add <= '9')
            {
                s.insert(0, 1, num1[i] + add);
                add = 0;
                i--;
            }
            else
            {
                s.insert(0, 1, num1[i] + add - 10);
                add = 1;
                i--;
            }
        }
        while (add == 1 && j >= 0)
        {
            if (num2[j] + add <= '9')
            {
                s.insert(0, 1, num2[j] + add);
                add = 0;
                j--;
            }
            else
            {
                s.insert(0, 1, num2[j] + add - 10);
                add = 1;
                j--;
            }
        }
        if (add == 1 && i < 0 && j < 0)
        {
            s.insert(0, 1, '0' + add);
        }
        while (i >= 0)
        {
            s.insert(0, 1, num1[i]);
            i--;
        }
        while (j >= 0)
        {
            s.insert(0, 1, num2[j]);
            j--;
        }

        return s;
    }
};