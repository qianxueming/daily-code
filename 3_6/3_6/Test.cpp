////#include <iostream>
////#include <errno.h>
////
////using namespace std;
////
////class Stack
////{
////public:
////	Stack(int n = 4)
////	{
////		cout << "Stack()" << endl;
////		int* tmp = (int*)malloc(sizeof(int) * n);
////		if (tmp == nullptr)
////		{
////			perror("malloc fail");
////		}
////		_a = tmp;
////		_size = 0;
////		_capacity = n;
////	}
////	~Stack()
////	{
////		cout << "~Stack()" << endl;
////		free(_a);
////		_a = nullptr;
////		_size = 0;
////		_capacity = 0;
////	}
////private:
////	int* _a;
////	int _size;
////	int _capacity;
////};
////
////class Queue
////{
////public:
////	Queue()
////	{
////		cout << "Queue()" << endl;
////		
////	}
////private:
////	Stack popst;
////	Stack pushst;
////};
////
//////int main()
//////{
//////	Queue q;
//////	return 0;
//////}
////
//////class Date
//////{
//////	friend 	ostream& operator<<(ostream& out, const Date& d);
//////public:
//////	Date(int year = 0 , int month = 0 , int day = 0 )//构造函数
//////		:_year(1),
//////		_month(1),
//////		_day(1),
//////		_a(1)
//////	{
//////		
//////	}
//////private:
//////	int _year;
//////	int _month;
//////	int _day;
//////	const int _a;
//////};
//////
//////ostream& operator<<(ostream& out, const Date& d)
//////{
//////	cout << d._year << "年" << d._month << "月" << d._day << "日" << endl;
//////	cout << "_a=" << d._a << endl;
//////
//////	return out;
//////}
//////
//////int main()
//////{
//////	//Date d1(2023, 3, 6);
//////	Date d1;
//////
//////	cout << d1;
//////
//////	return 0;
//////}
////
////
//////class A
//////{
//////public:
//////	A(int n)//不属于默认构造函数
//////	{
//////		_a = n;
//////	}
//////private:
//////	int _a;
//////};
//////
//////class B
//////{
//////public:
//////	B(int a)
//////		:aa(a)
//////	{
//////
//////	}
//////private:
//////	A aa;
//////};
////
//////int main()
//////{
//////	B bb(4);
//////
//////	return 0;
//////}
////
////
//////class A
//////{
//////public:
//////	A()
//////		:_a(1),
//////		_b(_a)
//////	{
//////
//////	}
//////private:
//////	int _b;
//////	int _a;
//////};
//////
//////int main()
//////{
//////	A aa;
//////	
//////	return 0;
//////}
////
//////class A
//////{
//////public:
//////	explicit A(int n = 1,int a = 1)
//////	{
//////		cout << "A（）" << endl;
//////		_a = n;
//////		_b = a;
//////	}
//////	A(const A& aa)
//////		:_a(aa._a)
//////		,_b(aa._b)
//////	{
//////		cout << "A(const A& aa)" << endl;
//////	}
//////private:
//////	int _a;
//////	int _b;
//////};
//////
//////int main()
//////{
//////
//////	//A aa1(1);//构造函数构造对象
//////
//////	A aa2 = { 5, 6 };//将1隐式类型转化成A类型然后拷贝构造给aa     
//////
//////	const A& ref = { 5, 6 };
//////
//////	return 0;
//////}
////
////
//////class A
//////{
//////public:
//////
//////	A(int a = 0)
//////	{
//////		count++;
//////		_a = a;
//////	}
//////	A(const A& a)
//////	{
//////		count++;
//////		_a = a._a;
//////	}
//////
//////	
//////
//////private:
//////static int count;
//////int _a;
//////};
//////
//////int A::count = 0;
//////
//////int main()
//////{
//////	A aa1;
//////	cout << aa1.count << endl;
//////	return 0;
//////}
////
//////int main()
//////{
//////	A aa(1);
//////	A aa1[10];
//////
//////	cout << aa.count << endl;
//////	cout << A::count << endl;
//////
//////	A* ptr = nullptr;
//////
//////	cout << (*ptr).count << endl;
//////
//////	return 0;
//////}
////
//////
//////class A
//////{
//////private:
//////	int _a = 1;
//////	static int i;
//////public:
//////	class B
//////	{
//////	public:
//////		void foo(const A& a)
//////		{
//////			cout << i << endl;//OK
//////			cout << a._a << endl;//OK
//////		}
//////	private:
//////
//////		int _b = 1;
//////	};
//////};
//////
//////int A::i = 0;
//////
//////int main()
//////{
//////	A a;
//////
//////	A::B bb;
//////
//////	bb.foo(a);
//////
//////	cout << sizeof(a) << endl;
//////
//////	//cout << sizeof(bb) << endl;
//////
//////	return 0;
//////}
////
////
////class A
////{
////public:
////	A(int a = 0)
////		:_a(a)
////	{
////		cout << "A(int a)" << endl;
////	}
////
////	A(const A& aa)
////		:_a(aa._a)
////	{
////		cout << "A(const A& aa)" << endl;
////	}
////
////	A& operator=(const A& aa)
////	{
////		cout << "A& operator=(const A& aa)" << endl;
////
////		if (this != &aa)
////		{
////			_a = aa._a;
////		}
////
////		return *this;
////	}
////
////	~A()
////	{
////		cout << "~A()" << endl;
////	}
////private:
////	int _a;
////};
////
////A func3()
////{
////	A aa;
////	return aa; 
////}
////
////A func4()
////{
////	return A();
////}
////
////int main()
////{
////	func3();
////
////	A aa1 = func3(); // 拷贝构造+拷贝构造  -- 优化为一个拷贝构造
////
////	cout << "****" << endl;
////	A aa2;
////	aa2 = func3();  // 不能优化
////
////	cout << "---------------------------" << endl;
////	func4(); // 构造+拷贝构造 -- 优化为构造
////	A aa3 = func4(); // 构造+拷贝构造+拷贝构造  -- 优化为构造
////
////	return 0;
////}
////
////
////void func1(A aa)
////{
////
////}
////void func2(const A& aa)
////{
////
////}
////
//////int main()
//////{
//////	A aa1 = 1;  // 构造+拷贝构造 -》 优化为直接构造
//////	//func1(aa1);
//////
//////	//func1(3);// 构造+拷贝构造 -》 优化为直接构造
//////	//func1(A(3));// 构造+拷贝构造 -》 优化为直接构造
//////	cout << "----------------------------------" << endl;
//////	func2(aa1);  // 无优化
//////	func2(2);    // 无优化
//////	func2(A(3)); // 无优化
//////
//////	return 0;
//////}
//
//#define  _CRT_SECURE_NO_WARNINGS 1
//
//#include<iostream>
//using namespace std;
//#include <stdio.h>
//
////int main()
////{
////	int a, b, c;
////	scanf("%4d%2d%2d", &a, &b, &c);
////	cout << a << " " << b << " " << c << endl;
////
////	return 0;
////}
//
//#include <iostream>
//using namespace std;
//
//class Date
//{
//public:
//    Date(int year, int month, int day)
//        :_year(year)
//        , _month(month)
//        , _day(day)
//    {}
//    int GetMonthday(int year, int month)
//    {
//        int arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//        if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
//        {
//            return 29;
//        }
//        else
//            return arr[month];
//    }
//    void operator+(int x)
//    {
//        _day += x;
//        while (_day > GetMonthday(_year, _month))
//        {
//            _day -= GetMonthday(_year, _month);
//            _month++;
//            if (_month == 13)
//            {
//                _year++;
//                _month = 1;
//            }
//        }
//    }
//private:
//    int _year;
//    int _month;
//    int _day;
//};
//
//int main() {
//    int year, month, day;
//    year = month = day = 0;
//    scanf("%4d%2d%2d", &year, &month, &day);
//    Date d1(year, month, day);
//    d1 + 500;
//    scanf("%4d%2d%2d", &year, &month, &day);
//    Date d2(year, month, day);
//
//
//    return 0;
//}

#include <iostream>
using namespace std;

int main() {
	int year, month, day;
	cin >> year >> month >> day;
	int arr[12] = { 0,31,59,90,120,151,181,212,243
	,273,304,334 };
	int nday = day;
	nday += arr[month - 1];
	if (month > 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
	{
		nday += 1;
	}
	return nday;
}
