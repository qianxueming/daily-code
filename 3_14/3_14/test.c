//leetcode 1051高度检查器
int heightChecker(int* heights, int heightsSize) {
    // if ( heightsSize == 0)//空数组
    // return 0;

    int max = heights[0];
    int min = heights[0];
    int i = 0;
    while (i < heightsSize)//取数组最大值和最小值确定区间范围
    {
        if (max < heights[i])
            max = heights[i];
        if (min > heights[i])
            min = heights[i];

        i++;
    }
    //申请数组存储各个数字出现次数
    int* tmp1 = (int*)calloc(sizeof(int), (max - min + 1));
    //统计出现次数
    for (int j = 0; j < heightsSize; j++)
    {
        tmp1[heights[j] - min]++;
    }
    //存储预期数组
    int* tmp2 = (int*)calloc(sizeof(int), (heightsSize));
    int k = 0;
    for (int j = 0; j < max - min + 1; j++)
    {
        while (tmp1[j] > 0)
        {
            tmp2[k] = j + min;
            k++;
            tmp1[j]--;
        }
    }
    int flag = 0;
    for (int j = 0; j < heightsSize; j++)
    {
        if (heights[j] != tmp2[j])
            flag++;
    }

    return flag;
}