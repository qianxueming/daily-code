#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()//参数解析
{
    vector<string> v;
    string s;
    getline(cin, s);

    //指令前有空格
    int i = 0;
    while (s[i] == ' ')
    {
        i++;
    }

    int j = i;
    for (; i < s.size(); i++)
    {
        if (s[i] == ' ')
        {
            string str(s.begin() + j, s.begin() + i);
            v.push_back(str);
            j = i + 1;
        }
        if (s[i] == '"')
        {
            int x = i + 1;
            int y = s.find('"', x + 1);
            if (y != -1)
            {
                string str(s.begin() + x, s.begin() + y);
                v.push_back(str);
            }
            //引号后续字符串未结束
            j = y + 2;
            i = y + 2;
        }
    }
    if (j < s.size())
    {
        string str(s.begin() + j, s.begin() + s.size());
        v.push_back(str);
    }
    cout << v.size() << endl;
    for (auto e : v)
    {
        cout << e << endl;
    }
    return 0;
}