//#include <iostream>
//
//using namespace  std;
//
//class A
//{
//public:
//	int _a;
//};
//
//class B //--只要是虚继承就会存一个地址记录继承的类的偏移量，这是编译器为了统一处理不管是否菱形继承但虚继承的方式
//{
//public:
//	int _b;
//};
//
//class C :public B,public A
//{
//public:
//	int _c;
//};
//
//class D
//{
//public:
//	int _d;
//};
//
//int main()
//{
//	C cc;
//	cc._a = 1;
//	cc._b = 2;
//	cc._c = 3;
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//class A
//{
//public:
//	int _a = 2;
//};
//
//class B:  A
//{
//public:
//	int _b = 1;
//};
//
//int main()
//{
//	B bb;
//	A aa;
//	aa = bb;
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//class book
//{
//public:
//	book(int i)
//		:qu(i)
//	    ,price(qu*10)
//	{}
//	void Print(int i)
//	{
//		printf("p[%d] is %d\n", i, qu * price);
//	}
//private:
//	int qu;
//	int price;
//};
//
//int main()
//{
//	book* pp = new book[5]{ 1, 2, 3, 4, 5 };
//	for (int i = 0; i < 5; i++)
//	{
//		pp[i].Print(i);
//	}
//	return 0;
//}
//
//#include <iostream>
//
//using namespace std;
//
//class cylinder
//{
//public:
//	cylinder(double rad, double height)
//	{
//		_rad = rad;
//		_height = height;
//		_volume = 3.14 * _rad *_rad * _height;
//	}
//	double Get_volume()
//	{
//		return _volume;
//	}
//private:
//	double _rad;
//	double _height;
//	double _volume;
//};
//
//int main()
//{
//	double rad, height;
//	cin >> rad >> height;
//	cylinder cy(rad, height);
//	cout << "volume is " << cy.Get_volume() << endl;
//	return 0;
//}

//#include <iostream>
//#include <string>
//#include <cstring>
//
//using namespace std;
//
//class guest
//{
//public:
//	guest(string name)
//	{
//		_name = name;
//		no = ++total;
//	}
//	void Print()
//	{
//		cout << "name= " << _name << " no= " << no << " total= " << total << endl;
//	}
//	static int total;
//private:
//	int no;
//	string _name;
//};
//
//int guest::total = 0;
//
//int main()
//{
//	guest g1("xdl");
//	guest g2("dbz");
//	guest g3("dcc");
//
//	string name;
//	cin >> name;
//	if (strcmp(name.c_str(), "xdl") == 0)
//	{
//		g1.Print();
//	}
//	else if (strcmp(name.c_str(), "dbz") == 0)
//	{
//		g2.Print();
//	}
//	else
//	{
//		g3.Print();
//	}
//	return 0;
//}

#include <iostream>

using namespace std;

class Person
{
public:
	virtual ~Person()
	{
		cout << "~Person()" << endl;
	}
};

class Student : public Person
{
public:
	virtual ~Student()
	{
		cout << "~Student()" << endl;
	}
};

int main()
{
	Person* ptr1 = new Person;
	Person* ptr2 = new Student;

	delete ptr1;
	delete ptr2;//由于指针类型是Person类型的，因此只会按照Person类型调用析构函数，但是没有调用派生类Student的析构函数，解决这个问题需要多态
	//只需要在派生类和基类的析构函数加上virtual关键字即可，因为析构函数都是处理同名的

	return 0;
}