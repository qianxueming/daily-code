void my_reserve(int* nums, int begin, int end)
{
    int left = begin;
    int  right = end;
    while (left < right)
    {
        int temp = nums[left];
        nums[left] = nums[right];
        nums[right] = temp;
        left++;
        right--;
    }

}

void rotate(int* nums, int numsSize, int k) {
    k %= numsSize;
    my_reserve(nums, 0, numsSize - k - 1);
    my_reserve(nums, numsSize - k, numsSize - 1);
    my_reserve(nums, 0, numsSize - 1);
}