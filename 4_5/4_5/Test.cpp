//#include <iostream>
//#include <vector>
//
//using namespace std;
//
//int main()
//{
//	vector<int> v;
//	v.resize(20);
//	v.reserve(3);
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//template<class T, int N = 10>//非类型模板参数 -- 整形常量
//class Array
//{
//public:
//	//Array(int n)
//	//{
//	//	N = n;//非类型模板参数为常量不能修改
//	//}
//private:
//	T A[N];
//};
//
//int main()
//{
//	//Array<int> arr(10);
//	Array<int> arr;
//	Array<int, 20> arr1;
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//void func();
//
//void func()
//{
//	cout << "func()" << endl;
//};//函数实际是能以分号结尾
//
//int main()
//{
//	func();
//	return 0;
//};
//
//
//#include <iostream>
//
//using namespace std;
//
////template<class T>//原函数模板
////bool Less(const T a, const T b)
////{
////	return a < b;
////}
////
////template<>//对原函数模板进行特化 -- 特化不能脱离原模版单独出现
////bool Less<int*>(int* a, int* b)
////{
////	return *a < *b;
////}
//////////////////////////////////////////////////////
////template<class T>//原函数模板
////bool Less(const T& a, const T& b)
////{
////	return a < b;
////}
//
////template<>//对原函数模板进行特化 -- error
////bool Less<int*>(int*& a, int*& b)
////{
////	return *a < *b;
////}
//
////template<>//对原函数模板进行特化 -- error
////bool Less<int*>(int* a, int* b)
////{
////	return *a < *b;
////}
//
//template<class T>//原类模板
//struct Less
//{
//	bool operator()(T a, T b)
//	{
//		return a < b;
//	}
//};
//
//template<>      // --- 类模板全特化 -- 全特化即是将模板参数列表中所有的参数都确定化。
//struct Less<int*>
//{
//	bool operator()(int* a, int* b)
//	{
//		return *a < *b;
//	}
//};
//
////偏特化
//template<class T1, class T2>//原模版
//class Data
//{
//public:
//	Data(T1 a, T2 b)
//	{
//		cout << "Data(T1 a, T2 b)" << endl;
//	}
//};
//
//template<class T>
//class Data<T,int>//将原模版一部分参数特化
//{
//public:
//	Data()
//	{
//		cout << "Data(int a, T b)" << endl;
//	}
//};
//
//template<class T1, class T2>// -- 对原模板模板类型参数进行调整
//class Data<T1*, T2*>
//{
//public:
//	Data()
//	{
//		cout << "Data<T1*, T2*>" << endl;
//	}
//};
//
//int main()
//{
//	int a = 1;
//	double b = 2;
//	Data<int, double> d1(a, b);//类模板使用时需要显示传入类型，函数模板会自行推断，如果推断失败便会报错
//	Data<double, int> d2;
//	Data<int*, double*>d3;
//	return 0;
//}

//int main()
//{
//	/*int a = 1;
//	int b = 2;
//	cout << Less(a, b) << endl;
//
//	int* c = new int(5);
//	int* d = new int(4);
//	cout << Less(c, d) << endl;*/
//
//	/*int a = 1;
//	int b = 2;
//	cout << Less<int>()(a, b) << endl;
//
//	int* c = new int(5);
//	int* d = new int(4);
//	cout << Less<int*>()(c, d) << endl;*/
//	return 0;
//};


//#include <iostream>
//#include <vector>
//#include <cstring>
//
//using namespace std;
//
//void Merge_Sort(vector<int>& v1, int begin, int end, vector<int>& v2)   //   -----  归并排序
//{
//	if (begin >= end)//如果长度小于等于1，认为区间有序
//		return;
//
//	int mid = (begin + end) / 2;//分割区间
//	int begin1 = begin, begin2 = mid + 1;
//	int end1 = mid, end2 = end;
//	Merge_Sort(v1, begin, mid, v2);
//	Merge_Sort(v1, mid + 1, end, v2);
//
//	int  i = begin;
//	while (begin1 <= end1 && begin2 <= end2)
//	{
//		if (v1[begin1] < v1[begin2])
//		{
//			v2[i++] = v1[begin1++];
//		}
//		else
//		{
//			v2[i++] = v1[begin2++];
//		}
//	}
//
//	while (begin1 <= end1)
//	{
//		v2[i++] = v1[begin1++];
//	}
//	while (begin2 <= end2)
//	{
//		v2[i++] = v1[begin2++];
//	}
//
//	memcpy(&v1[begin], &v2[begin], sizeof(int) * (end - begin + 1));
//}
//
//int main()
//{
//	int n = 0;
//	cin >> n;
//	vector<int> v1(n);
//	vector<int> v2(n);
//	for (auto& e : v1)
//	{
//		int num = 0;
//		cin >> num;
//		e = num;
//	}
//	Merge_Sort(v1, 0, v2.size()-1, v2);
//	for (auto e : v1)
//	{
//		cout << e << ' ';
//	}
//	return 0;
//}

//
//#include <iostream>
//#include <cstring>
//
//using namespace std;
//
//void Merge_Sort(int* arr, int begin, int end, int* temp)
//{
//	if (begin >= end)
//		return;
//
//	int mid = (begin + end) / 2;
//	int begin1 = begin, begin2 = mid + 1;
//	int end1 = mid, end2 = end;
//
//	Merge_Sort(arr, begin1, end1, temp);
//	Merge_Sort(arr, begin2, end2, temp);
//
//	int i = begin;
//	while (begin1 <= end1 && begin2 <= end2)
//	{
//		if (arr[begin1] < arr[begin2])
//		{
//			temp[i++] = arr[begin1++];
//		}
//		else
//		{
//			temp[i++] = arr[begin2++];
//		}
//	}
//	while (begin1 <= end1)
//	{
//		temp[i++] = arr[begin1++];
//	}
//	while (begin2 <= end2)
//	{
//		temp[i++] = arr[begin2++];
//	}
//
//	memcpy(arr + begin, temp + begin, sizeof(int) * (end - begin + 1));
//}
//
//
//int main()
//{
//	int n = 0;
//	cin >> n;
//
//	int* arr = new int[n];
//	int* temp = new int[n];
//
//	for (int i = 0; i < n; i++)
//	{
//		int num = 0;
//		cin >> num;
//		arr[i] = num;
//	}
//
//	Merge_Sort(arr, 0, n-1, temp);
//
//	for (int i = 0; i < n; i++)
//	{
//		cout << arr[i] << ' ';
//	}
//
//	return 0;
//}

//#include <iostream>
//#include <vector>
//#include <cstring>
//
//using namespace std;
//
//int main()
//{
//	vector<int> v1(5);
//	vector<int> v2 = { 1,2,3,4,5 };
//
//	memcpy(&v1[0], &v2[0], sizeof(int) * 5);
//
//	for (auto e : v1)
//	{
//		cout << e << ' ';
//	}
//
//	return 0;
//}

//#include <iostream>
//#include <vector>
//#include <algorithm>
//
//using namespace std;
//
//int Find_Max_Num(vector<int>& v,int begin, int end)
//{
//	if (begin == end)//只剩一个数字认为是区间最大
//		return v[begin];
//
//	int mid = (begin + end) / 2;
//	int begin1 = begin, end1 = mid;
//	int begin2 = mid + 1, end2 = end;
//
//	int max1 = Find_Max_Num(v, begin1, end1);
//	int max2 = Find_Max_Num(v, begin2, end2);
//
//	return max(max1, max2);
//}
//
//int main()
//{
//	int n;
//	cin >> n;
//	vector<int> v(n);
//	for (auto& e : v)
//	{
//		int num = 0;
//		cin >> num;
//		e = num;
//	}
//
//	cout << Find_Max_Num(v, 0 , n-1) << endl;
//
//	return 0;
//}

//#include <iostream>
//#include <vector>
//
//using namespace std;
//
//int n = 0;
////棋盘覆盖问题
//void ChessBoard(vector<vector<int>>& v, int tx, int ty, int dx, int dy, int size)//tx，ty表示左上角的行号和列号，dx,dy表示特殊点的行号和列号，size表示键盘的长（宽）
//{
//	if (size == 1)//如果棋盘大小是1X1认为已经覆盖完
//		return;
//
//	int num = ++n;//记录此次需要填入的数
//	size = size / 2;//每次长(宽)减半
//	//左上棋盘
//	if (dx >= tx && dx < tx + size && dy >= ty && dy < ty + size)//如果子棋盘内有被覆盖点
//	{
//		ChessBoard()
//	}
//	else//如果子棋盘内没有被覆盖点
//	{
//
//	}
//}
//
//int main()
//{
//	int size, dx, dy;
//	cin >> size >> dx >> dy;
//	vector<vector<int>> v(size+1, vector<int>(size+1));
//	v[dx][dy] = num;
//	ChessBoard(v, 1, 1, dx, dy, size);
//	return 0;
//}
//
//#include <iostream>
//using namespace std;
//
//
//int tile = 1;        // 骨牌序号
//
//int board[128][128]; // 二维数组模拟棋盘
//
//
//
//// (tr,tc)表示棋盘的左上角坐标(即确定棋盘位置), (dr,dc)表示特殊方块的位置, size=2^k确定棋盘大小
//
//void chessBoard(int tr, int tc, int dr, int dc, int size)
//
//{
//
//    // 递归出口
//
//    if (size == 1)
//
//        return;
//
//    int s = size / 2; //分割棋盘
//
//    int t = tile++;   //t记录本层骨牌序号
//
//    // 判断特殊方格在不在左上棋盘
//
//    if (dr < tr + s && dc < tc + s)
//
//    {
//
//        chessBoard(tr, tc, dr, dc, s); //特殊方格在左上棋盘的递归处理方法
//
//    }
//
//    else
//
//    {
//
//        board[tr + s - 1][tc + s - 1] = t;             // 用t号的L型骨牌覆盖右下角
//
//        chessBoard(tr, tc, tr + s - 1, tc + s - 1, s); // 递归覆盖其余方格
//
//    }
//
//    // 判断特殊方格在不在右上棋盘
//
//    if (dr < tr + s && dc >= tc + s)
//
//    {
//
//        chessBoard(tr, tc + s, dr, dc, s);
//
//    }
//
//    else
//
//    {
//
//        board[tr + s - 1][tc + s] = t;
//
//        chessBoard(tr, tc + s, tr + s - 1, tc + s, s);
//
//    }
//
//    // 判断特殊方格在不在左下棋盘
//
//    if (dr >= tr + s && dc < tc + s)
//
//    {
//
//        chessBoard(tr + s, tc, dr, dc, s);
//
//    }
//
//    else
//
//    {
//
//        board[tr + s][tc + s - 1] = t;
//
//        chessBoard(tr + s, tc, tr + s, tc + s - 1, s);
//
//    }
//
//
//
//    // 判断特殊方格在不在右下棋盘
//
//    if (dr >= tr + s && dc >= tc + s)
//
//    {
//
//        chessBoard(tr + s, tc + s, dr, dc, s);
//
//    }
//
//    else
//
//    {
//
//        board[tr + s][tc + s] = t;
//
//        chessBoard(tr + s, tc + s, tr + s, tc + s, s);
//
//    }
//
//}
//
//
//
//int main()
//
//{
//
//    int boardSize;                 // 棋盘边长
//
//    int dr, dc;
//
//    cin >> boardSize >> dr >> dc;
//
//    chessBoard(0, 0, dr-1, dc-1, boardSize); // (0, 0)为顶点，大小为boardSize的棋盘； 特殊方块位于(3, 3)
//
//    // 打印棋盘
//
//    int i, j;
//
//    for (i = 0; i < boardSize; i++)
//
//    {
//        //cout << ' ';
//        for (j = 0; j < boardSize; j++)
//
//        {
//
//            printf("%2d ", board[i][j]);
//
//        }
//
//        printf("\n");
//
//    }
//
//    return 0;
//
//}

#include <iostream>

using namespace std;

#define N 128

int title = 1;
int board[N][N];

//tr,tc表示左上角的行号和列号，dr,dc表示特殊符号的行号和列号，size表示棋盘长(宽)
void ChessBoard(int tr, int tc, int dr, int dc, int size)
{
	if (size == 1)
		return;

	int t = title++;//记录本次所写入数据
	int s = size / 2;//表示子棋盘的长/宽

	if (dr < tr + s && dc < tc + s)//判断覆盖位置是否在左上棋盘内
	{
		ChessBoard(tr, tc, dr, dc, s);
	}
	else
	{
		board[tr + s - 1][tc + s - 1] = t;
		ChessBoard(tr, tc, tr + s - 1, tc + s - 1, s);
	}

	if (dr < tr + s && dc >= tc + s)//判断覆盖位置是否在右上棋盘内
	{
		ChessBoard(tr, tc + s, dr, dc, s);
	}
	else
	{
		board[tr + s - 1][tc + s] = t;
		ChessBoard(tr, tc + s, tr + s - 1, tc + s , s);
	}

	if (dr >= tr + s && dc < tc + s)//判断覆盖位置是否在左下棋盘内
	{
		ChessBoard(tr + s, tc , dr, dc, s);
	}
	else
	{
		board[tr + s][tc + s - 1] = t;
		ChessBoard(tr + s, tc , tr + s , tc + s - 1, s);
	}

	if (dr >= tr + s && dc >= tc + s)//判断覆盖位置是否在右下棋盘内
	{
		ChessBoard(tr + s, tc + s, dr, dc, s);
	}
	else
	{
		board[tr + s][tc + s] = t;
		ChessBoard(tr + s, tc + s, tr + s , tc + s , s);
	}
}

int main()
{
	int dr, dc, size;
	cin >> size >> dr >> dc;
	board[dr - 1][dc - 1] = 0;

	ChessBoard(0, 0, dr-1, dc-1, size);

	for (int i = 0; i < size; i++)
	{
		cout << ' ';
		for (int j = 0; j < size; j++)
		{
			/*cout << board[i][j] << ' ';*/
			printf("%2d ", board[i][j]);
		}
		cout << endl;
	}
	return 0;
}