#pragma once

#include <iostream>
#include <vector>
using namespace std;


template<size_t N>
class bitset
{
public:
	bitset()
	{
		_bits.resize(N / 8 + 1, 0);
	}
	//实现位运算时只需要看高低位，不需要在意硬件的存储结构--设定每个字节的 高位 存储的数据是 较大 的

	void set(size_t x)
	{
		size_t i = x / 8;
		size_t j = x % 8;

		_bits[i] |= (1 << j);
	}

	void reset(size_t x)
	{
		size_t i = x / 8;
		size_t j = x % 8;

		_bits[i] &= ~(1 << j);
	}

	bool test(size_t x)
	{
		size_t i = x / 8;
		size_t j = x % 8;

		return _bits[i] & (1 << j);
	}
private:
	vector<char> _bits;
};


void test_bitset1()
{
	bitset<100> bits;

	bits.set(6);
	bits.set(7);
	bits.set(8);

	cout << bits.test(6) << endl;
	cout << bits.test(7) << endl;
	cout << bits.test(8) << endl;

	bits.reset(8);
	bits.reset(7);
	bits.reset(6);

	cout << bits.test(6) << endl;
	cout << bits.test(7) << endl;
	cout << bits.test(8) << endl;
}

void test_bitset2()
{
	bitset<-1> bt1;
	bitset<0xFFFFFFFF> bt2;

}


template<size_t N>
class twobitset
{
public:
	void set(size_t x)
	{
		// 00没出现
		// 01出现一次
		// 10出现一次以上

		if (_bt1.test(x) == false
			&& _bt2.test(x) == false)
		{
			// 00 -> 01
			_bt1.reset(x);
			_bt2.set(x);
		}
		else if (_bt1.test(x) == false
			&& _bt2.test(x) == true)
		{
			// 01 -> 10
			_bt1.set(x);
			_bt2.reset(x);
		}
	}
	void Print()
	{
		for (size_t i = 0; i < N; i++)
		{
			if (_bt1.test(i) == false
				&& _bt2.test(i) == true)
			{
				cout << i << ' ';
			}
		}
		cout << endl;
	}

private:
	bitset<N> _bt1;
	bitset<N> _bt2;
};

void test_twobitset()
{
	int arr[] = { 3,7,5, 4,5,6,2,36,4,23,4,2, 5, 4, 5,2,3 ,6,8,8,7,36};
	twobitset<100> bt;
	for (auto e : arr)
	{
		bt.set(e);
	}
	bt.Print();
}

struct BKDRHash
{
	size_t operator()(const string& s)
	{
		size_t hash = 0;
		for (auto ch : s)
		{
			hash += ch;
			hash *= 31;
		}

		return hash;
	}
};

struct APHash
{
	size_t operator()(const string& s)
	{
		size_t hash = 0;
		for (size_t i = 0; i < s.size(); i++)
		{
			size_t ch = s[i];
			if ((i & 1) == 0)
			{
				hash ^= ((hash << 7) ^ ch ^ (hash >> 3));
			}
			else
			{
				hash ^= (~((hash << 11) ^ ch ^ (hash >> 5)));
			}
		}
		return hash;
	}
};


struct DJBHash
{
	size_t operator()(const string& s)
	{
		size_t hash = 5381;
		for (auto ch : s)
		{
			hash += (hash << 5) + ch;
		}
		return hash;
	}
};

template<size_t N, 
class K = string, 
class Hash1 = BKDRHash, 
class Hash2 = APHash,
class Hash3 = DJBHash>
	class BloomFilter
{
public:
	void set(const K& key)
	{
		size_t len = N * _X;
		size_t hash1 = Hash1()(key) % len;
		_bt.set(hash1);

		size_t hash2 = Hash2()(key) % len;
		_bt.set(hash2);

		size_t hash3 = Hash3()(key) % len;
		_bt.set(hash3);

		cout << hash1 << ":" << hash2 << ":" << hash3 << endl;
	}

	bool test(const K& key)
	{
		size_t len = N * _X;
		size_t hash1 = Hash1()(key) % len;
		if (!_bt.test(hash1))
		{
			return false;
		}

		size_t hash2 = Hash2()(key) % len;
		if (!_bt.test(hash2))
		{
			return false;
		}

		size_t hash3 = Hash3()(key) % len;
		if (!_bt.test(hash3))
		{
			return false;
		}

		return true;
	}
private:
	static const size_t _X = 4;
	bitset<N * _X> _bt;
};

void test_bloomfilter()
{
	BloomFilter<10> bf;
	bf.set("string");
	bf.set("hello world");
	bf.set("sort");
	bf.set("test");
	bf.set("stet");
	bf.set("etst");

	cout << bf.test("string") << endl;
	cout << bf.test("hello world") << endl;
	cout << bf.test("sort") << endl;
	cout << bf.test("test") << endl;
	cout << bf.test("stet") << endl;
	cout << bf.test("etst") << endl;
	cout << bf.test("estt") << endl;
	cout << bf.test("ttes") << endl;
	cout << bf.test("this") << endl;
	cout << bf.test("is") << endl;
	cout << bf.test("bloomfiltertest") << endl;
}