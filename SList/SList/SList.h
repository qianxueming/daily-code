#define  _CRT_SECURE_NO_WARNINGS 1

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>

typedef int SLTDataType;

typedef struct SListNode
{
	SLTDataType data;
	struct SListNode* next;
}SLTNode;

SLTNode* BuySLTNode(SLTDataType x);//申请新的结点

void SLTPushBack(SLTNode** pphead, SLTDataType x);//尾插

void SLTPushFront(SLTNode** pphead, SLTDataType x);//头插

void SLTPopBack(SLTNode** pphead);//尾删
//void SLTPopBack(SLTNode* phead);


void SLTPopFront(SLTNode** pphead);//头删

void SLTPrint(SLTNode* phead);//打印数据

SLTNode* SLTFind(SLTNode* phead,SLTDataType x);//查找

void SLTInsertAfter(SLTNode* pos, SLTDataType x);//在pos位置后面插入结点

void SLTInsert(SLTNode** pphead, SLTNode* pos, SLTDataType x);//在pos位置之前插入结点

void SLTEraseAfter(SLTNode* pos);//删除pos位置后的结点

void SLTErase(SLTNode** pphead, SLTNode* pos);//删除pos位置的结点

void SLTDestory(SLTNode** pphead);//链表的释放