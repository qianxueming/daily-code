#include "List.h"

void test01()
{
	LTNode* phead = LTInit();

	LTPushBack(phead, 1);
	LTPushBack(phead, 2);
	LTPushBack(phead, 3);
	LTPushBack(phead, 4);
	LTPushBack(phead, 5);

	LTPrint(phead);

	LTPopBack(phead);
	LTPrint(phead);

	LTPopBack(phead);
	LTPrint(phead);

	LTPopBack(phead);
	LTPrint(phead);

	LTPopBack(phead);
	LTPrint(phead);

	LTPopBack(phead);
	LTPrint(phead);

	

}

void test02()
{
	LTNode* phead = LTInit();
	LTPushFront(phead, 5);
	LTPushFront(phead, 4);
	LTPushFront(phead, 3);
	LTPushFront(phead, 2);
	LTPushFront(phead, 1);

	LTPrint(phead);
	printf("%d\n", LTSize(phead));
	printf("%d\n", LTEmpty(phead));
	
	LTPopFront(phead);
	LTPrint(phead);
	LTPopFront(phead);
	LTPrint(phead);
	LTPopFront(phead);
	LTPrint(phead);

	LTPopFront(phead);
	LTPrint(phead);

	LTPopFront(phead);
	LTPrint(phead);
	printf("%d\n", LTEmpty(phead));
	LTDestroy(phead);
	phead = NULL;
}

void test03()
{
	LTNode* phead = LTInit();
	LTPushBack(phead, 1);
	LTPushBack(phead, 2);
	LTPushBack(phead, 3);
	LTPushBack(phead, 4);
	LTPushBack(phead, 5);

	LTPrint(phead);

	LTNode* pos = LTFind(phead, 5);
	/*if (pos)
	{
		pos->data *= 10;;
	}*/
	//LTInsert(pos, 66);
	LTErase(pos);
	LTPrint(phead);

}

int main()
{
	test02();


	return 0;
}