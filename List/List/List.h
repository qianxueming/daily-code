#pragma once

#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdbool.h>

typedef int LTDataType;

typedef struct ListNode
{
	struct ListNode* prev;
	struct ListNode* next;
	LTDataType data;
}LTNode;

LTNode* BuyListNode(LTDataType x);//申请新的结点

LTNode* LTInit();//初始化

void LTPushBack(LTNode* phead,LTDataType x);//尾插

void LTPopBack(LTNode* phead);//尾删

void LTPushFront(LTNode* phead, LTDataType x);//头插

void LTPopFront(LTNode* phead);//头删

void LTPrint(LTNode* phead);//打印数据

LTNode* LTFind(LTNode* phead, LTDataType x);//查找数据

void LTInsert(LTNode* pos, LTDataType x);//在pos之前插入x 

void LTErase(LTNode* pos);//删除pos位置

bool LTEmpty(LTNode* phead);//判空

size_t LTSize(LTNode* phead);//查询链表结点个数

void LTDestroy(LTNode* phead);//链表销毁