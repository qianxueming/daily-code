#include "List.h"

LTNode* BuyListNode(LTDataType x)//申请新的结点
{
	LTNode* newnode = (LTNode*)malloc(sizeof(LTNode));
	
	if (newnode == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	
	newnode->data = x;
	newnode->prev = NULL;
	newnode->next = NULL;

	return newnode;
}


LTNode* LTInit()//初始化
{
	LTNode* phead = BuyListNode(-1);
	
	phead->next = phead;
	phead->prev = phead;

	return phead;
}


void LTPushBack(LTNode* phead,LTDataType x)//尾插
{
	assert(phead);

	/*LTNode* newnode = BuyListNode(x);
	LTNode* tail = phead->prev;
	
	tail->next = newnode;
	newnode->prev = tail;
	
	phead->prev = newnode;
	newnode->next = phead;*/

	//利用LTInsert实现尾插
	LTInsert(phead, x);
}


void LTPopBack(LTNode* phead)//尾删
{
	assert(phead);
	assert(phead->next != phead);//空链表

	/*LTNode* tail = phead->prev;
	LTNode* tailPrev = tail->prev;

	tailPrev->next = phead;
	phead->prev = tailPrev;

	free(tail);*/

	//利用LTErase实现尾删
	LTErase(phead->prev);
}


void LTPushFront(LTNode* phead, LTDataType x)//头插
{
	assert(phead);

	//LTNode* newnode = BuyListNode(x);

	//////顺序有关
	////newnode->next = phead->next;
	////phead->next->prev = newnode;

	////newnode->prev = phead;
	////phead->next = newnode;

	////顺序无关
	//LTNode* first = phead->next;

	//phead->next = newnode;
	//newnode->prev = phead;
	//newnode->next = first;
	//first->prev = newnode;

	//利用LTInsert实现头插
	LTInsert(phead->next, x);
}

void LTPopFront(LTNode* phead)//头删
{
	assert(phead);
	assert(phead->next != phead);//判空

	/*LTNode* first = phead->next;
	LTNode* second = first->next;

	free(first);

	phead->next = second;
	second->prev = phead;*/

	//利用LTErase实现头删
	LTErase(phead->next);
}

void LTPrint(LTNode* phead)//打印数据
{
	assert(phead);

	LTNode* cur = phead->next;

	while (cur != phead)
	{
		printf("%d ", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

LTNode* LTFind(LTNode* phead, LTDataType x)//查找数据
{
	assert(phead);
	assert(phead->next != phead);

	LTNode* cur = phead->next;
	while (cur != phead)
	{
		if (cur->data == x)
			return cur;

		cur = cur->next;
	}
	return NULL;
}

void LTInsert(LTNode* pos, LTDataType x)//在pos之前插入x 
{
	assert(pos);

	LTNode* newnode = BuyListNode(x);
	LTNode* prev = pos->prev;

	prev->next = newnode;
	newnode->prev = prev;
	newnode->next = pos;
	pos->prev = newnode;
}

void LTErase(LTNode* pos)//删除pos位置
{
	assert(pos);

	LTNode* prev = pos->prev;
	LTNode* next = pos->next;

	free(pos);

	prev->next = next;
	next->prev = prev;
}

bool LTEmpty(LTNode* phead)//判空
{
	assert(phead);

	return phead->next == phead;
}

size_t LTSize(LTNode* phead)
{
	assert(phead);

	LTNode* cur = phead->next;
	size_t size = 0;

	while (cur != phead)
	{
		size++;
		cur = cur->next;
	}

	return size;
}

void LTDestroy(LTNode* phead)//链表销毁(使用完手动置空哨兵位)
{
	assert(phead);

	LTNode* cur = phead->next;
	while (cur != phead)
	{
		LTNode* next = cur->next;

		free(cur);

		cur = next;
	}

	free(phead);
}