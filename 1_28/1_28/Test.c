#define  _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>

struct TreeNode
{
    char val;
    struct TreeNode* left;
    struct TreeNode* right;
};

struct TreeNode* rebuildingTree(char* str, int* pi)
{
    if (*str == '#')
    {
        (*pi)++;
        return NULL;
    }

    struct TreeNode* root = (struct TreeNode*)malloc(sizeof(struct TreeNode));
    root->val = str[(*pi)++];

    root->left = rebuildingTree(str, pi);
    root->right = rebuildingTree(str, pi);

    return root;
}

void InOrder(struct TreeNode* root)
{
    if (root == NULL)
    {
        return;
    }

    InOrder(root->left);
    printf("%c ", root->val);
    InOrder(root->right);

}

int main() {
    char str[100];
    scanf("%s", str);
    int i = 0;
    struct TreeNode* root = rebuildingTree(str, &i);
    InOrder(root);
    return 0;
}