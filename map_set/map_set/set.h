#pragma once

#include "RBTree.h"

namespace qxm
{
	template<class K>
	class set
	{
		struct SetOfT
		{
			const K& operator()(const K& key)
			{
				return key;
			}
		};
	public:
		/*typedef __RBTreeIterator<K, K&, K*> iterator;
		typedef __RBTreeIterator<K, const K&, K*> const_iterator;*/

		typedef typename RBTree<K, K, SetOfT>::const_iterator iterator;
		typedef typename RBTree<K, const K, SetOfT>::const_iterator const_iterator;

		iterator begin()
		{
			return _t.begin();
		}

		iterator end()
		{
			return _t.end();
		}

		pair<iterator, bool> Insert(const K& key)
		{
			return _t.Insert(key);
		}

	private:
		RBTree<K, K, SetOfT> _t;
	};

	void test_set1()
	{
		int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
		set<int> s;
		for (auto e : a)
		{
			s.Insert(e);
		}

		set<int>::iterator it = s.begin();
		while (it != s.end())
		{
			//*it = 666;
			cout << *it << ' ';
			++it;
		}
		cout << endl;

		for (auto e : s)
		{
			cout << e << ' ';
		}
		cout << endl;
	}
}
