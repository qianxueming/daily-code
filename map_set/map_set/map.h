﻿#pragma once

#include "RBTree.h"

namespace qxm
{
	template<class K, class V>
	class map
	{
		struct MapOfT
		{
			const K& operator()(const pair<K, V>& kv)
			{
				return kv.first;
			}
		};
	public:
	/*	typedef __RBTreeIterator<V, V&, V*> iterator; */

		typedef typename RBTree<K, pair<const K, V>, MapOfT>::iterator iterator;

		iterator begin()
		{
			return _t.begin();
		}

		iterator end()
		{
			return _t.end();
		}

		V& operator[](const K& key)
		{
			pair<iterator, bool> ret = _t.Insert(make_pair(key, V()));
			return ret.first->second;
		}

		pair<iterator, bool> Insert(const pair<const K, V>& kv)
		{
			return _t.Insert(kv);
		}
	private:
		RBTree<K, pair<const K, V>, MapOfT> _t;
	};

	void test_map1()
	{

		map<string, string> dict;
		dict.Insert(make_pair("sort", "排序"));
		dict.Insert(make_pair("string", "字符串"));
		dict.Insert(make_pair("count", "计数"));
		dict.Insert(make_pair("count", "(计数)")); // ʧ

		map<string, string>::iterator it = dict.begin();

		while (it != dict.end())
		{
			//it->second = "666";
			cout << it->first << ":" << it->second << endl;
			++it;
		}
		cout << endl;
	}

	void test_map2()
	{
		string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
		map<string, int> countMap;

		for (auto& e : arr)
		{
			countMap[e]++;
		}

		for (auto& kv : countMap)
		{
			cout << kv.first << ":" << kv.second << endl;
		}
	}
}
