#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>

typedef int HPDataType;

typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}HP;

void HeapPrint(HP* php);

void HeapInit(HP* php);
void HeapDestroy(HP* php);
void HeapPush(HP* php, HPDataType x);

void HeapPop(HP* php);//ɾ���Ѷ�Ԫ��
HPDataType HeapTop(HP* php);//��ȡ�Ѷ�Ԫ��

int HeapSize(HP* php);
// �ѵ��п�
bool HeapEmpty(HP* php);

// �ѵĹ���
void HeapCreate(HP* php, HPDataType* a, int n);

void HeapSort(HP* php, HPDataType* a, int n);//������

void TopK(HP* php, HPDataType* a, int n, int k);