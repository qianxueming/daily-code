#include "Heap.h"

void HeapPrint(HP* php)
{
	assert(php);
	for (int i = 0; i < php->size; i++)
	{
		printf("%d ", php->a[i]);
	}
	printf("\n");
}

void Swap(HPDataType* p1, HPDataType* p2)
{
	HPDataType tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

void AdjustUp(HPDataType* a, int child)//向上调整
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[child] > a[parent])//建立大堆
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void AdjustDown(HPDataType* a, int n, int parent)//向下调整
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		//确保指向的是大的那个孩子
		if (child + 1 < n && a[child + 1] > a[child])//大堆的调整
		{
			child++;
		}
		//如果父亲小于儿子，进行调整
		if (a[child] > a[parent])
		{
			Swap(&a[parent], &a[child]);
			parent = child;
			child = 2 * parent + 1;
		}
		//如果父亲大于儿子，调整结束
		else
		{
			break;
		}
	}
}

void HeapInit(HP* php)
{
	assert(php);

	php->a = NULL;
	php->size = php->capacity = 0;
}

void HeapDestroy(HP* php)
{
	assert(php);

	free(php->a);
	php->a = NULL;
	php->size = php->capacity = 0;
}

void HeapPush(HP* php, HPDataType x)
{
	assert(php);

	if (php->size == php->capacity)//扩容
	{
		int newcapacity = php->capacity == 0 ? 4 : php->capacity * 2;
		HPDataType* tmp = (HPDataType*)realloc(php->a, sizeof(HPDataType) * newcapacity);
		if (tmp == NULL)
		{
			perror("realloc fail");
			exit(-1);
		}
		php->a = tmp;
		php->capacity = newcapacity;
	}

	php->a[php->size] = x;
	php->size++;
	AdjustUp(php->a, php->size - 1);
}


void HeapPop(HP* php)
{
	assert(php);
	assert(php->size > 0);

	Swap(&php->a[0], &php->a[php->size - 1]);//堆顶元素和堆最后一个元素交换
	php->size--;

	AdjustDown(php->a, php->size, 0);
}

HPDataType HeapTop(HP* php)//获取堆顶元素
{
	assert(php);
	assert(php->size > 0);

	return php->a[0];
}

int HeapSize(HP* php)
{
	assert(php);

	return php->size;
}

bool HeapEmpty(HP* php)
{
	assert(php);

	return php->size == 0;
}

void HeapCreate(HP* php, HPDataType* a, int n)
{
	assert(php);

	php->a = (HPDataType*)malloc(sizeof(HPDataType) * n);
	if (php->a == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	/*for (int i = 0; i < n; i++)
	{
		php->a[i] = a[i];
	}*/
	memcpy(php->a, a, sizeof(HPDataType) * n);
	php->size = php->capacity = n;

	//向下调整建堆
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(php->a, n, i);
	}
	////向上调整建堆
	//for (int i = 0; i < n; i++)
	//{
	//	AdjustUp(php->a, i);
	//}
}


void HeapSort(HP* php, HPDataType* a, int n)//堆排序
{
	assert(php);

	HeapCreate(php, a, n);

	int end = n - 1;
	while (end > 0)
	{
		Swap(&php->a[0], &php->a[end]);
		AdjustDown(php->a, end, 0);
		end--;
	}
}

void TopK(HP* php, HPDataType* a, int n, int k)//TopK问题
{
	assert(php);

	php->a = (HPDataType*)malloc(sizeof(HPDataType)*k);
	php->size = php->capacity = k;

	for (int i = 0; i < k; i++)
	{
		php->a[i] = a[i];
	}
	AdjustDown(php->a, k, 0);

	for (int i = k; i < n; i++)
	{
		if (php->a[0] < a[i])
		{
			php->a[0] = a[i];
		}
		AdjustDown(php->a, k, 0);
	}
}