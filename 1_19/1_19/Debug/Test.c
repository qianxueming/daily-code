#include "Heap.h"

void test01()
{
	int array[] = { 27,15,19,18,28,34,65,49,25,37 };

	HP hp;
	HeapInit(&hp);
	for (int i = 0; i < sizeof(array) / sizeof(array[0]); i++)
	{
		HeapPush(&hp, array[i]);
	}
	HeapPrint(&hp);
	for (int i = 0; i <9 ; i++)
	{
		//printf("%d\n", HeapTop(&hp));
		HeapPop(&hp);
	}
	printf("%d\n", HeapSize(&hp));
	if (HeapEmpty(&hp))
		printf("empty\n");
	HeapDestroy(&hp);
}

int main()
{
	test01();
	return 0;
}