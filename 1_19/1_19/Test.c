#include "Heap.h"

void test01()
{
	int array[] = { 27,15,19,18,28,34,65,49,25,37 };

	HP hp;
	HeapInit(&hp);
	for (int i = 0; i < sizeof(array) / sizeof(array[0]); i++)
	{
		HeapPush(&hp, array[i]);
	}
	HeapPrint(&hp);
	for (int i = 0; i <9 ; i++)
	{
		//printf("%d\n", HeapTop(&hp));
		HeapPop(&hp);
	}
	printf("%d\n", HeapSize(&hp));
	if (HeapEmpty(&hp))
		printf("empty\n");
	HeapDestroy(&hp);
}

void test02()
{
	HP hp;
	int array[] = { 27,15,19,18,28,34,65,49,25,37 };
	HeapCreate(&hp, array, sizeof(array) / sizeof(int));


	HeapPrint(&hp);
	HeapDestroy(&hp);
}

void test03()
{
	HP hp;
	int array[] = { 27,15,19,18,28,34,65,49,25,37 };

	HeapSort(&hp, array, sizeof(array) / sizeof(array[0]));

	HeapPrint(&hp);
}

void test04()
{
	HP hp;
	HeapInit(&hp);
	int array[] = { 27,15,19,18,28,34,65,49,25,37 };
	TopK(&hp, array, sizeof(array)/sizeof(array[0]), 5);
	HeapPrint(&hp);
	HeapDestroy(&hp);

}

int main()
{
	test04();
	return 0;
}