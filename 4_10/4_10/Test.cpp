//class Solution {
//public:
//    int longestCommonSubsequence(string text1, string text2) {
//        vector<vector<int>> dp(text1.size() + 1, vector<int>(text2.size() + 1));
//
//        for (int i = 0; i <= text1.size(); i++)
//            dp[i][0] = 0;
//        for (int i = 0; i <= text2.size(); i++)
//            dp[0][i] = 0;
//
//        for (int i = 0; i < text1.size(); i++)
//        {
//            for (int j = 0; j < text2.size(); j++)
//            {
//                if (text1[i] == text2[j])
//                {
//                    dp[i + 1][j + 1] = dp[i][j] + 1;
//                }
//                else
//                {
//                    dp[i + 1][j + 1] = max(dp[i + 1][j], dp[i][j + 1]);
//                }
//            }
//        }
//
//        return dp[text1.size()][text2.size()];
//    }
//};


//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        vector<int> dp(nums.size());
//        dp[0] = nums[0];
//        for (int i = 1; i < nums.size(); i++)
//        {
//            dp[i] = max(dp[i - 1] + nums[i], nums[i]);
//        }
//        int ret = dp[0];
//        for (auto e : dp)
//        {
//            if (ret < e)
//            {
//                ret = e;
//            }
//        }
//        return ret;
//    }
//};