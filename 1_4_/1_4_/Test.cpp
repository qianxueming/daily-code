#include <stdio.h>
#include <stdlib.h>

//int a = 10;//全局域内变量
//
//void func1()
//{
//	int a = 0;//局部域内变量
//	printf("%d\n", a);
//}
//
//void func2()
//{
//	int a = 1;//局部域内变量
//	printf("%d\n", a);
//	printf("%d\n", ::a);//加上::只从全局域寻找a变量，::表示全局域
//
//}
//
//int main()
//{
//	func1();
//	func2();
//
//	return 0;
//}

//int rand = 10;
//
//int main()
//{
//	printf("%d\n", rand);
//
//	return 0;
//}







//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	cout << "hello world!" << endl;
//
//	return 0;
//}



//#include "Queue.h"
//
//using namespace AQueue;
//
//
//int main()
//{
//	struct Node node1;
//
//	return 0;
//}

//#include <iostream>
//
////using namespace std;
//using std::cout;
//using std::endl;
//
//int main()
//{
//	cout << "hello world!" << endl;
//
//	return 0;
//}


#include <iostream>

using namespace std;

void Func(int a = 10, int b = 20, int c = 30)
{
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;
	cout << "c = " << c << endl;
}

//struct Stack
//{
//	int* a;
//	int top;
//	int capacity;
//};
//
//void StackInit(struct Stack* ps, int defaultCapacity = 4)
//{
//	ps->a = (int*)malloc(sizeof(int) * defaultCapacity);
//	if (ps->a == NULL)
//	{
//		perror("malloc fail");
//		exit(-1);
//	}
//	ps->top = 0;
//	ps->capacity = defaultCapacity;
//}

int main()
{
	Func(1,2,3);
	cout << endl;
	Func(1,2);
	cout << endl;
	Func(1);
	cout << endl;
	Func();

	return 0;
}