#pragma once
#include <iostream>
using namespace std;


namespace key
{
	template<class K>
	struct BSTreeNode
	{
		BSTreeNode(K key)
		{
			_key = key;
			_left = nullptr;
			_right = nullptr;
		}
		BSTreeNode* _left;
		BSTreeNode* _right;
		K _key;
	};

	//由于会产生歪脖子树的情况，因此增删改查的时间复杂度是O(N）
	template<class K>
	class BSTree
	{
		typedef struct BSTreeNode<K> Node;
	public:
		/*BSTree()
			:_root(nullptr)
		{

		}*/
		BSTree() = default;//C++11强制生成默认构造函数

		BSTree(const BSTree<K>& t)//拷贝构造
		{
			_root = Copy(t._root);
		}

		BSTree<K>& operator=(BSTree<K> t)
		{
			swap(_root, t._root);
			return *this;
		}

		~BSTree()
		{
			Destory(_root);
			//_root = nullptr;
		}
		bool Insert(const K& key)//插入函数
		{
			if (_root == nullptr)
			{
				_root = new Node(key);
			}
			else
			{
				Node* newnode = new Node(key);
				Node* prev = nullptr;
				Node* cur = _root;
				while (cur)
				{
					if (newnode->_key < cur->_key)
					{
						prev = cur;
						cur = cur->_left;
					}
					else if (newnode->_key > cur->_key)
					{
						prev = cur;
						cur = cur->_right;
					}
					else
					{
						return false;
					}
				}
				if (newnode->_key < prev->_key)
				{
					prev->_left = newnode;
				}
				else
				{
					prev->_right = newnode;
				}
			}
			return true;
		}

		bool Find(const K& key)
		{
			if (_root == nullptr)
				return false;

			Node* cur = _root;
			while (cur)
			{
				if (cur->_key > key)
				{
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					cur = cur->_right;
				}
				else
				{
					return true;
				}
			}
			return  false;
		}

		bool Erase(const K& key)//删除某一个节点
		{
			/*	if (_root == nullptr)
					return false;*/

			Node* prev = nullptr;
			Node* cur = _root;
			while (cur)//寻找要删除的节点
			{
				if (cur->_key > key)
				{
					prev = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					prev = cur;
					cur = cur->_right;
				}
				else//寻找到要删除的节点
				{
					if (cur->_left == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_right;
						}
						else
						{
							if (cur == prev->left)
							{
								prev->_left = cur->_right;
							}
							else
							{
								prev->_right = cur->_right;
							}
						}
						delete cur;
					}
					else if (cur->_right == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_left;
						}
						else
						{
							if (cur == prev->left)
							{
								prev->_left = cur->_left;
							}
							else
							{
								prev->_right = cur->_left;
							}
						}
						delete cur;
					}
					else
					{
						//使用右子树的最小节点做替换
						Node* pminRight = cur;
						Node* minRight = cur->_right;
						while (cur->left)
						{
							pminRight = minRight;
							minRight = minRight->_left;
						}
						cur->_key = minRight->_key;
						if (pminRight->_right = minRight)
						{
							pminRight->_right = minRight->_right;
						}
						else
						{
							pminRight->_left = minRight->_right;
						}
						delete minRight;
					}
					return true;
				}
			}
			return false;
		}

		void InOrder()
		{
			_InOrder(_root);
			cout << endl;
		}

		bool FindR(const K& key)//递归查找
		{
			return _FindR(_root, key);
		}

		bool InsertR(const K& key)//递归插入
		{
			return _InsertR(_root, key);
		}

		bool EraseR(const K& key)//递归删除
		{
			return _EraseR(_root, key);
		}
	protected:

		Node* Copy(Node* root)
		{
			if (root == nullptr)
				return nullptr;

			Node* newnode = new Node(root->_key);
			newnode->_left = Copy(root->_left);
			newnode->_right = Copy(root->_right);

			return newnode;
		}

		void Destory(Node*& root)
		{
			if (root == nullptr)
				return;

			Destory(root->_left);
			Destory(root->_right);

			delete root;
			root = nullptr;
		}

		bool _FindR(Node* root, const K& key)
		{
			if (root == nullptr)
				return false;

			if (root->_key > key)
			{
				return _FindR(root->_left, key);
			}
			else if (root->_key < key)
			{
				return _FindR(root->_right, key);
			}
			else
			{
				return true;
			}
		}

		bool _InsertR(Node*& root, const K& key)
		{
			if (root == nullptr)
			{
				root = new Node(key);
				return true;
			}

			if (root->_key > key)
			{
				return _InsertR(root->_left, key);
			}
			else if (root->_key < key)
			{
				return _InsertR(root->_right, key);
			}
			else
			{
				return false;
			}
		}

		bool _EraseR(Node*& root, const K& key)
		{
			if (root == nullptr)
				return false;

			if (root->_key > key)
			{
				_EraseR(root->_left, key);
			}
			else if (root->_key < key)
			{
				_EraseR(root->_right, key);
			}
			else
			{
				Node* del = root;

				if (root->_left == nullptr)
				{
					root = root->_right;
				}
				else if (root->_right == nullptr)
				{
					root = root->_left;
				}
				else
				{
					Node* maxLeft = root->_left;
					while (maxLeft->_right)
					{
						maxLeft = maxLeft->_right;
					}

					swap(maxLeft->_key, root->_key);

					//_EraseR(root->_left, key);//???-error往下执行会把根结点delete
					return _EraseR(root->_left, key);//由于是左树的最大值因此一定满足右边为空，因此可以转换成子问题调用，注意不能传入maxLeft，因此那样引用的就是maxLeft变量而不是应该引用的树的节点的指针
				}
				delete del;
				return true;
			}
		}

		void _InOrder(Node* root)
		{
			if (root == nullptr)
				return;
			_InOrder(root->_left);
			cout << root->_key << " ";
			_InOrder(root->_right);
		}
	private:
		Node* _root = nullptr;
	};
}

namespace key_value
{
	template<class K, class V>
	struct BSTreeNode
	{
		BSTreeNode(K key, V value)
		{
			_key = key;
			_left = nullptr;
			_right = nullptr;
			_value = value;
		}
		BSTreeNode* _left;
		BSTreeNode* _right;
		K _key;
		V _value;
	};

	//由于会产生歪脖子树的情况，因此增删改查的时间复杂度是O(N）
	template<class K, class V>
	class BSTree
	{
		typedef struct BSTreeNode<K, V> Node;
	public:
		/*BSTree()
			:_root(nullptr)
		{

		}*/
		BSTree() = default;//C++11强制生成默认构造函数

		BSTree(const BSTree<K, V>& t)//拷贝构造
		{
			_root = Copy(t._root);
		}

		BSTree<K, V>& operator=(BSTree<K, V> t)
		{
			swap(_root, t._root);
			return *this;
		}

		~BSTree()
		{
			Destory(_root);
			//_root = nullptr;
		}
		bool Insert(const K& key, const V& value)//插入函数
		{
			if (_root == nullptr)
			{
				_root = new Node(key, value);
			}
			else
			{
				Node* newnode = new Node(key, value);
				Node* prev = nullptr;
				Node* cur = _root;
				while (cur)
				{
					if (newnode->_key < cur->_key)
					{
						prev = cur;
						cur = cur->_left;
					}
					else if (newnode->_key > cur->_key)
					{
						prev = cur;
						cur = cur->_right;
					}
					else
					{
						return false;
					}
				}
				if (newnode->_key < prev->_key)
				{
					prev->_left = newnode;
				}
				else
				{
					prev->_right = newnode;
				}
			}
			return true;
		}

		Node* Find(const K& key)
		{
			if (_root == nullptr)
				return nullptr;

			Node* cur = _root;
			while (cur)
			{
				if (cur->_key > key)
				{
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					cur = cur->_right;
				}
				else
				{
					return cur;
				}
			}
			return  nullptr;
		}

		bool Erase(const K& key)//删除某一个节点
		{
			/*	if (_root == nullptr)
					return false;*/

			Node* prev = nullptr;
			Node* cur = _root;
			while (cur)//寻找要删除的节点
			{
				if (cur->_key > key)
				{
					prev = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					prev = cur;
					cur = cur->_right;
				}
				else//寻找到要删除的节点
				{
					if (cur->_left == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_right;
						}
						else
						{
							if (cur == prev->left)
							{
								prev->_left = cur->_right;
							}
							else
							{
								prev->_right = cur->_right;
							}
						}
						delete cur;
					}
					else if (cur->_right == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_left;
						}
						else
						{
							if (cur == prev->left)
							{
								prev->_left = cur->_left;
							}
							else
							{
								prev->_right = cur->_left;
							}
						}
						delete cur;
					}
					else
					{
						//使用右子树的最小节点做替换
						Node* pminRight = cur;
						Node* minRight = cur->_right;
						while (cur->left)
						{
							pminRight = minRight;
							minRight = minRight->_left;
						}
						cur->_key = minRight->_key;
						if (pminRight->_right = minRight)
						{
							pminRight->_right = minRight->_right;
						}
						else
						{
							pminRight->_left = minRight->_right;
						}
						delete minRight;
					}
					return true;
				}
			}
			return false;
		}

		void InOrder()
		{
			_InOrder(_root);
			cout << endl;
		}

	protected:

		Node* Copy(Node* root)
		{
			if (root == nullptr)
				return nullptr;

			Node* newnode = new Node(root->_key);
			newnode->_left = Copy(root->_left);
			newnode->_right = Copy(root->_right);

			return newnode;
		}

		void Destory(Node*& root)
		{
			if (root == nullptr)
				return;

			Destory(root->_left);
			Destory(root->_right);

			delete root;
			root = nullptr;
		}

		void _InOrder(Node* root)
		{
			if (root == nullptr)
				return;
			_InOrder(root->_left);
			cout << root->_key << ":" << root->_value << endl;;
			_InOrder(root->_right);
		}
	private:
		Node* _root = nullptr;
	};
}