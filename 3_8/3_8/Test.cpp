//#include <iostream>
//
//using namespace std;
//
////int main()
////{
////	//申请一个int类型的空间
////	//C语言方式
////	//int* ptr1 = (int*)malloc(sizeof(int));
////	//C++方式
////	int* ptr1 = new int;
////
////	//申请十个int类型的数组的空间
////	//C语言方式
////	//int* ptr1 = (int*)malloc(sizeof(int) * 10);
////	//C++方式
////	int* ptr2 = new int[10];
////
////	//释放一个int类型的空间
////	//C语言方式
////	//free(ptr1);
////	//C++方式
////	delete ptr1;
////	
////	//释放int类型数组的空间
////	//C语言方式
////	//free(ptr2);
////	//C++方式
////	delete []ptr2;
////
////	//申请一个int类型的空间并初始化
////	//C语言方式
////	//int* ptr3 = (int*)calloc(1, sizeof(int));
////	//C++方式
////	int* ptr3 = new int(0);
////
////	//申请十个int类型的数组的空间并初始化
////	//C语言方式
////	//int* ptr4 = (int*)calloc(1, sizeof(int));
////	//C++方式
////	int* ptr4 = new int[10]{0,1,2,3,4,5,6,7,8,9};
////	return 0;
////}
//
//
//class A
//{
//public:
//	A(int a = 0)
//		:_a(a)
//	{
//		cout << "A(int a = 0)" << endl;
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _a;
//};
//
//
//int main()
//{
//	A* aa = (A*)malloc(sizeof(A));
//	if (aa == nullptr)
//	{
//		perror("malloc fail");
//		exit(-1);
//	}
//
//	new(aa)A;
//
//	aa->~A();
//
//	free(aa);
//
//	return 0;
//}
//// 
//// 
////int main()
////{
////	A* aa = new A;
////
////	delete aa;
////
////	A* aa1 = new A[10];
////
////	delete[] aa1;
////	return 0;
////}
//
////int main()
////{
////	// new/delete 和 malloc/free最大区别是 new/delete对于【自定义类型】除了开空间还会调用构造函数和析构函数
////	A* p1 = (A*)malloc(sizeof(A));
////	A* p2 = new A(1);
////	free(p1);
////	delete p2;
////	// 内置类型是几乎是一样的
////	int* p3 = (int*)malloc(sizeof(int)); // C
////	int* p4 = new int;
////	free(p3);
////	delete p4;
////	A* p5 = (A*)malloc(sizeof(A) * 10);
////	A* p6 = new A[10];
////	free(p5);
////	delete[] p6;
////
////	return 0;
////}
//
//
////int main()
////{
////	try
////	{
////		int size = 0;
////		while (1)
////		{
////			int* a = new int[1024 * 1024 * 100];
////			size += 1024 * 1024 * 500;
////			cout << size / 1024 / 1024 << "MB" << endl;
////		}
////	}
////	catch (exception& e)
////	{
////		cout << e.what() << endl;
////	}
////	return 0;
////}
//
////
////int main()
////{
////	
////		int size = 0;
////		while (1)
////		{
////			int* a = new int[1024 * 1024 * 100];
////			size += 1024 * 1024 * 500;
////			cout << size / 1024 / 1024 << "MB" << endl;
////		}
////	
////	return 0;
////}


#include <iostream>
using namespace std;

int GetMonthDay(int year, int month)
{
    int arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };

    if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
    {
        return 29;
    }

    return arr[month];
}

int main() {
    int year = 0, month = 1, day = 0;
    int nday = 0;

    while (cin >> year >> nday)
    {
        month = 1, day = 0;



        while (nday > GetMonthDay(year, month))
        {
            month++;
            if (month == 13)
            {
                year++;
                month = 1;
            }
            nday -= GetMonthDay(year, month);
        }
        day += nday;
        printf("%04d-%02d-%02d\n", year, month, day);
    }
    return 0;
}
