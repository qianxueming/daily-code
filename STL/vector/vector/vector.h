#pragma once

//#include <vector>
//#include <iostream>
#include <algorithm>
using namespace std;
#include <cassert>
//#include <cstring>

namespace qxm
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;
		vector(); //默认构造函数
		vector(size_t n, const T& val = T());
		vector(int n, const T& val = T());
		template <class InputIterator>
		vector(InputIterator first, InputIterator last); //迭代器初始化
		vector(const vector<T>& v);//拷贝构造
		~vector();//析构函数
		
		iterator begin();//返回指向数据开头的迭代器
		iterator end(); //返回指向最后一个数据的下一个数据的位置的迭代器
		const_iterator begin() const;//const迭代器版本
		const_iterator end() const;//const迭代器版本

		size_t size()const; //获取容器内有效数据的个数
		size_t capacity()const;//获取容器容量
		void reserve(size_t n); //扩容函数
		void resize(size_t n, T val = T());//扩容函数
		bool empty();//检查容器是否为空

		void push_back(const T& x); //尾插函数
		void pop_back();//尾删函数
		iterator insert(iterator pos, const T& val);//插入函数
		iterator erase(iterator pos);//删除函数

		T& operator[](size_t n); //[]重载
		const T& operator[](size_t n) const; //const变量版本[]重载
		vector<T>& operator=(const vector<T>& v);//传统写法
		//vector<T>& operator=(vector<T> v);//现代写法

		void swap(vector<T>& v);//交换函数

	private:
		iterator _start;  //指向数据开头的位置
		iterator _finish; //指向最后一个有效数据的下一个数据的位置
		iterator _end_of_storage; //指向容量空间的末尾位置
	};

	template<class T>
	vector<T>::vector() 
		:_start(nullptr),
		_finish(nullptr),
		_end_of_storage(nullptr)
	{}

	template<class T>
	vector<T>::vector(size_t n, const T& val)
		: _start(nullptr),
		_finish(nullptr),
		_end_of_storage(nullptr)
	{
		reserve(n);//提前扩容，提高效率
		for (size_t i = 0; i < n; ++i)
		{
			push_back(val);
		}
	}

	template<class T>
	vector<T>::vector(int n, const T& val)
		: _start(nullptr),
		_finish(nullptr),
		_end_of_storage(nullptr)
	{
		reserve(n);//提前扩容，提高效率
		for (int i = 0; i < n; ++i)
		{
			push_back(val);
		}
	}

	template<class T>
	template <class InputIterator>
	vector<T>::vector(InputIterator first, InputIterator last)//指向数组的原生指针可看作迭代器，可以作为参数传入
		: _start(nullptr),
		_finish(nullptr),
		_end_of_storage(nullptr)
	{
		while (first != last)
		{
			push_back(*first);
			++first;
		}
	}

	//template<class T>
	//vector<T>::vector(const vector<T>& v)
	//{
	//	//传统写法
	//	_start = new T[v.capacity()];
	//	for (size_t i = 0; i < v.size(); ++i)
	//	{
	//		_start[i] = v._start[i];
	//	}
	//	_finish = _start + v.size();
	//	_end_of_storage = _start + v.capacity();
	//}

	template<class T>
	vector<T>::vector(const vector<T>& v)
		: _start(nullptr),
		_finish(nullptr),
		_end_of_storage(nullptr)
	{
		//现代写法
		vector<T> tmp(v.begin(), v.end());//用迭代器初始化创建一个和v数据相同的vector
		swap(tmp);
	}

	template<class T>
	vector<T>::~vector()
	{
		delete[] _start;
		_start = _finish = _end_of_storage = nullptr;
	}

	template<class T>
	typename vector<T>::iterator vector<T>::begin()
	{
		return _start;
	}

	template<class T>
	typename vector<T>::iterator vector<T>::end()
	{
		return _finish;
	}

	template<class T>
	typename vector<T>::const_iterator vector<T>::begin() const
	{
		return _start;
	}

	template<class T>
	typename vector<T>::const_iterator vector<T>::end() const
	{
		return _finish;
	}

	template<class T>
	size_t vector<T>::size()const 
	{
		return _finish - _start;
	}
	
	template<class T>
	size_t vector<T>::capacity()const
	{
		return _end_of_storage - _start;
	}

	template<class T>
	void vector<T>::reserve(size_t n)
	{
		if (n > capacity())
		{
			T* tmp = new T[n]; //申请能存储n个T类型数据的空间
			size_t sz = size();//提前保存原有size大小，后续size函数会失效
			if (_start) //如果原有空间存在，需要拷贝原有空间的数据
			{
				//memcpy(tmp, _start, sizeof(T) * size()); //将原有数据拷贝至新空间
				for (size_t i = 0; i < sz; ++i)
				{
					tmp[i] = _start[i];
				}
				delete[] _start; //将原有空间释放
				//delete _start; ///error
			}
			_start = tmp;//指向新空间的数据开头
			_finish = _start + sz; //指向新空间最后一个有效数据的下一个位置
			_end_of_storage = _start + n;//指向新空间的末尾位置
		}
	}

	template<class T>
	void vector<T>::resize(size_t n, T val)
	{
		if (n < size())//n是无符号整形，不用担心n为负数造成_finish指向位置为非法位置
		{
			_finish = _start + n;
		}
		else
		{
			if (n > capacity())//n大于capacity需要扩容
			{
				reserve(n);
			}
			while (_finish != _start + n)//将原有有效数据位置到第n个数据位置设置为val
			{
				*_finish = val;
				++_finish;
			}
		}
	}


	template<class T>
	bool vector<T>::empty()
	{
		return _finish == _start;
	}

	template<class T>
	void vector<T>::push_back(const T& x)
	{
		if (_finish == _end_of_storage) //容器已满，需要扩容
		{
			size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2; //扩容空间至原有空间的2倍
			reserve(newcapacity);
		}
		//容器未满，直接尾插
		*_finish = x;
		++_finish;
	}

	template<class T>
	void vector<T>::pop_back()
	{
		assert(!empty());//检查是否有数据
		--_finish;
	}

	template<class T>
	typename vector<T>::iterator vector<T>::insert(iterator pos, const T& val)
	{
		assert(pos >= _start);//检查插入位置是否越界
		assert(pos <= _finish);

		size_t len = pos - _start;//记录偏移量
		if (_finish == _start)//容器已满
		{
			size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
			reserve(newcapacity);
			pos = _start + len;//使得pos指向新空间的相应位置
		}

		iterator end = _finish - 1;
		while (pos <= end)
		{
			*(end + 1) = *end;
			--end;
		}

		*pos = val;
		++_finish;

		return pos;
	}

	template<class T>
	typename vector<T>::iterator vector<T>::erase(iterator pos)
	{
		assert(pos >= _start);//判断删除位置是否越界
		assert(pos < _finish);

		iterator start = pos + 1;
		while (start != _finish)
		{
			*(start - 1) = *start;
			++start;
		}

		--_finish;

		return pos;
	}

	template<class T>
	T& vector<T>::operator[](size_t n)
	{
		assert(n < size()); //检查是否越界访问
		return _start[n];
	}

	template<class T>
	const T& vector<T>::operator[](size_t n) const
	{
		return _start[n];
	}

	template<class T>
	vector<T>& vector<T>::operator=(const vector<T>& v)
	{
		if (_start != v._start) //避免v1 = v1出错
		{
			delete[] _start;
			_start = new T[v.capacity()];

			for (size_t i = 0; i < v.size(); ++i)
			{
				_start[i] = v._start[i];
			}
			_finish = _start + v.size();
			_end_of_storage = _start + v.capacity();
		}
		return *this;
	}

	//template<class T>
	//vector<T>& vector<T>::operator=(vector<T> v)
	//{
	//	//现代写法
	//	swap(v);
	//	return *this;
	//}
	
	template<class T>
	void vector<T>::swap(vector<T>& v)
	{
		std::swap(_start, v._start);
		std::swap(_finish, v._finish);
		std::swap(_end_of_storage, v._end_of_storage);
	}

	//////////////////test_func/////////////////////////////////////////////////////////////////////////////
	//void func(const vector<int>& v1)
	//{
	//	for (size_t i = 0; i < v1.size(); ++i)
	//	{
	//		cout << v1[i] << " ";
	//	}
	//	cout << endl;

	//	vector<int>::const_iterator it = v1.begin();
	//	while (it != v1.end())
	//	{
	//		cout << *it << " ";
	//		++it;
	//	}
	//	cout << endl;

	//	for (auto it : v1)
	//	{
	//		cout << it << " ";
	//	}
	//	cout << endl;
	//}

	//void test_vector1()
	//{
	//	vector<int> v1;
	//	v1.push_back(1);
	//	v1.push_back(2);
	//	v1.push_back(3);
	//	v1.push_back(4);
	//	v1.push_back(1);
	//	v1.push_back(2);
	//	v1.push_back(3);
	//	v1.push_back(1);
	//	v1.push_back(2);
	//	v1.push_back(3);
	//	v1.push_back(4);
	//	v1.push_back(4);

	//	func(v1);
	//	cout << endl << endl;

	//	vector<int>::iterator it = v1.begin();
	//	while (it != v1.end())
	//	{
	//		cout << *it << " ";
	//		++it;
	//	}
	//	cout << endl;

	//	for (auto it : v1)
	//	{
	//		cout << it << " ";
	//	}
	//	cout << endl;
	//}

	//void test_vector2()
	//{
	//	std::vector<int> v;
	//	v.push_back(1);
	//	v.push_back(2);
	//	v.push_back(3);
	//	v.push_back(4);
	//	v.push_back(5);

	//	cout << v.size() << endl;
	//	cout << v.capacity() << endl;
	//	auto  pos = find(v.begin(), v.end(), 1);
	//	pos = v.insert(pos, 66);
	//	cout << v.size() << endl;
	//	cout << v.capacity() << endl;
	//	for (auto e : v)
	//	{
	//		cout << e << " ";
	//	}
	//	cout << endl;

	//	(*pos)--;
	//	for (auto e : v)
	//	{
	//		cout << e << " ";
	//	}
	//	cout << endl;
	//}

	//void test_vector3()
	//{
	//	vector<int> v;
	//	v.push_back(1);
	//	v.push_back(2);
	//	v.push_back(3);
	//	v.push_back(4);
	//	vector<int>::iterator pos = find(v.begin(), v.end(), 1);
	//	v.insert(pos, 66);
	//	for (auto e : v)
	//	{
	//		cout << e << " ";
	//	}
	//	cout << endl;
	//	v.insert(pos, 6); //扩容使得迭代器pos失效，程序报错
	//	for (auto e : v)
	//	{
	//		cout << e << " ";
	//	}
	//}

	//void test_vector4()
	//{
	//	std::vector<int> v;
	//	v.push_back(1);
	//	v.push_back(2);
	//	v.push_back(3);
	//	v.push_back(4);
	//	std::vector<int>::iterator pos = find(v.begin(), v.end(), 2);
	//	v.erase(pos);
	//	for (auto e : v)
	//	{
	//		cout << e << " ";
	//	}
	//	cout << endl;
	//	*(pos) += 10;
	//	for (auto e : v)
	//	{
	//		cout << e << " ";
	//	}
	//	cout << endl;
	//}

	//class Solution {
	//public:
	//	vector<vector<int>> generate(int numRows) {
	//		vector<vector<int>> vv;
	//		vv.resize(numRows, vector<int>());
	//		for (size_t i = 0; i < vv.size(); ++i)
	//		{
	//			vv[i].resize(i + 1, 0);
	//			vv[i][0] = vv[i][vv[i].size() - 1] = 1;
	//		}

	//		for (size_t i = 0; i < vv.size(); ++i)
	//		{
	//			for (size_t j = 0; j < vv[i].size(); ++j)
	//			{
	//				if (vv[i][j] == 0)
	//				{
	//					vv[i][j] = vv[i - 1][j] + vv[i - 1][j - 1];
	//				}
	//			}
	//		}

	//		return vv;
	//	}
	//};


	//void test_vector5()
	//{
	//	vector<int> v1(10, 5);
	//	for (auto e : v1)
	//	{
	//		cout << e << " ";
	//	}
	//	cout << endl;

	//	vector<int> v2(v1);
	//	for (auto e : v2)
	//	{
	//		cout << e << " ";
	//	}
	//	cout << endl;

	//	vector<std::string> v3(3, "111111111111111111111");
	//	for (auto e : v3)
	//	{
	//		cout << e << " ";
	//	}
	//	cout << endl;

	//	vector<std::string> v4(v3);
	//	for (auto e : v4)
	//	{
	//		cout << e << " ";
	//	}
	//	cout << endl;

	//	v4.push_back("2222222222222222222");
	//	v4.push_back("2222222222222222222");
	//	v4.push_back("2222222222222222222");
	//	for (auto e : v4)
	//	{
	//		cout << e << " ";
	//	}
	//	cout << endl;

	//	vector<vector<int>> ret = Solution().generate(5);
	//	for (size_t i = 0; i < ret.size(); ++i)
	//	{
	//		for (size_t j = 0; j < ret[i].size(); ++j)
	//		{
	//			cout << ret[i][j] << " ";
	//		}
	//		cout << endl;
	//	}
	//	cout << endl;
	//}
}

