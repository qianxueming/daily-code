#pragma once

namespace qxm
{
	template<class T, class Container = vector<T>>
	class stack
	{
	public:
		void push(const T& x); //将数据压入栈中
		void pop(); //将栈顶数据弹出
		const T& top();	//获取栈顶数据
		size_t size();	//获取栈的数据个数
		bool empty();	//判断栈是否为空
	private:
		Container _con;
	};
}

template<class T, class Container>
void qxm::stack<T, Container>::push(const T& x)
{
	_con.push_back(x);
}

template<class T, class Container>
void qxm::stack<T, Container>::pop()
{
	_con.pop_back();
}

template<class T, class Container>
const T& qxm::stack<T, Container>::top()
{
	return _con.back();
}

template<class T, class Container>
size_t qxm::stack<T, Container>::size()
{
	return _con.size();
}

template<class T, class Container>
bool qxm::stack<T, Container>::empty()
{
	return _con.empty();
}


///////////////////test_func/////////////////////////////
void test_stack()
{
	qxm::stack<int, vector<int>> st1;
	qxm::stack<int, list<int>> st2;
	qxm::stack<int> st3;
	st1.push(1);
	st1.push(2);
	st1.push(3);
	st1.push(4);

	while (!st1.empty())
	{
		cout << st1.top() << " ";
		st1.pop();
	}
	cout << endl;
}