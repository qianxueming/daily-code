#pragma once

namespace qxm
{
	template<class T, class Container=list<T>>
	class queue
	{
	public:
		void push(const T& x); //将数据入队
		void pop(); //将栈顶数据出队
		const T& front();	//获取队头数据
		const T& back();	//获取队尾数据
		size_t size();	//获取栈的数据个数
		bool empty();	//判断栈是否为空
	private:
		Container _con;
	};
}

template<class T, class Container>
void qxm::queue<T, Container>::push(const T& x)
{
	_con.push_back(x);
}

template<class T, class Container>
void qxm::queue<T, Container>::pop()
{
	_con.pop_front();
}

template<class T, class Container>
const T& qxm::queue<T, Container>::front()
{
	return _con.front();
}

template<class T, class Container>
const T& qxm::queue<T, Container>::back()
{
	return _con.back();
}

template<class T, class Container>
size_t qxm::queue<T, Container>::size()
{
	return _con.size();
}

template<class T, class Container>
bool qxm::queue<T, Container>::empty()
{
	return _con.empty();
}



///////////////////test_func/////////////////////////////
void test_queue()
{
	qxm::queue<int, list<int>> q;
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);

	while (!q.empty())
	{
		cout << q.front() << " ";
		q.pop();
	}
	cout << endl;

}