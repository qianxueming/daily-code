#pragma once
//#include <iostream>
#include <algorithm>
using namespace std;

#include <assert.h>

namespace qxm
{
	template<class T>
	struct list_node //结点结构 --ListNode为类名，加上模板参数后为类型
	{
		list_node* _prev;
		list_node* _next;
		T _data;

		list_node(const T& val = T()) //结点的构造函数
		{
			_data = val;
			_prev = nullptr;
			_next = nullptr;
		}
	};

	template<class T, class Ref, class Ptr>
	struct __list_iterator
	{
		typedef list_node<T> node; //对结点重命名
		typedef __list_iterator<T, Ref, Ptr> self; //对迭代器重命名

		node* _node;

		__list_iterator(node* n)
			:_node(n)
		{}

		__list_iterator(const __list_iterator<T, T&, T*>& it)//--只在调用构造时会转化
			:_node(it._node)
		{}

		__list_iterator(const __list_iterator<T, const T&, const T*>& it)
			:_node(it._node)
		{}

		self& operator++()//前置++
		{
			_node = _node->_next;
			return *this;
		}

		self operator++(int)//后置++
		{
			self tmp(_node);
			_node = _node->_next;
			return tmp;
		}

		self& operator--()//前置--
		{
			_node = _node->_prev;
			return *this;
		}

		self operator--(int)//后置--
		{
			self tmp(_node);
			_node = _node->_prev;
			return tmp;
		}

		Ref operator*() //传引用返回可以修改结点内的数据
		{
			return _node->_data;
		}

		Ptr operator->()
		{
			return &_node->_data;
		}

		bool operator!=(const self& s)
		{
			return _node != s._node;
		}

		bool operator==(const self& s)
		{
			return _node == s._node;
		}
	};

	//template<class T>
	//struct __list_const_iterator
	//{
	//	typedef list_node<T> node; //对结点重命名
	//	typedef __list_const_iterator<T> self; //对迭代器重命名

	//	node* _node;

	//	__list_const_iterator(node* n)
	//		:_node(n)
	//	{}

	//	self& operator++()//前置++
	//	{
	//		_node = _node->_next;
	//		return *this;
	//	}

	//	self operator++(int)//后置++
	//	{
	//		self tmp(_node);
	//		_node = _node->_next;
	//		return tmp;
	//	}

	//	self& operator--()//前置--
	//	{
	//		_node = _node->_prev;
	//		return *this;
	//	}

	//	self operator--(int)//后置--
	//	{
	//		self tmp(_node);
	//		_node = _node->_prev;
	//		return tmp;
	//	}

	//	const T& operator*() 
	//	{
	//		return _node->_data;
	//	}

	//	bool operator!=(const self& s)
	//	{
	//		return _node != s._node;
	//	}

	//	bool operator==(const self& s)
	//	{
	//		return _node == s._node;
	//	}
	//};

	template<class T>
	class list
	{
		typedef list_node<T> node; //对结点重命名
	public:
		typedef __list_iterator<T, T&, T*> iterator; //普通迭代器
		typedef __list_iterator<T, const T&, const T*> const_iterator; //const迭代器

	public:
		void empty_init();

		list(); //默认构造函数
		template<class Iteartor>
		list(Iteartor begin, Iteartor end);
		void swap(list<T>& tmp);
		list(const list<T>& l);
		list<T>& operator=(list<T> l);
		~list();

		iterator begin();
		iterator end();

		const_iterator begin()const;
		const_iterator end()const;

		void push_back(const T& x);
		void insert(iterator pos, const T x);
		void push_front(const T& x);
		iterator erase(iterator pos);
		void pop_back();
		void pop_front();
		void clear();
	private:
		node* _head;
	};
}

template<class T>
void qxm::list<T>::empty_init()
{
	_head = new node;
	_head->_prev = _head;
	_head->_next = _head;
}


template<class T>
qxm::list<T>::list()
{
	empty_init();
}

template<class T>
template<class Iteartor>
qxm::list<T>::list(Iteartor begin, Iteartor end)
{
	empty_init();
	while (begin != end)
	{
		push_back(*begin);
		++begin;
	}
}

//template<class T>
//qxm::list<T>::list(const list<T>& l)
//{
//	//传统写法
//	empty_init();
//	const_iterator it = l.begin();
//	while (it != l.end())
//	{
//		push_back(*it);
//		it++;
//	}
//}

template<class T>
void qxm::list<T>::swap(list<T>& tmp)
{
	std::swap(_head, tmp._head);
}

template<class T>
qxm::list<T>::list(const list<T>& l)
{
	//现代写法
	empty_init();
	list<T> tmp(l.begin(), l.end());
	swap(tmp);
}

template<class T>
qxm::list<T>& qxm::list<T>::operator=(list<T> l)
{
	swap(l);
	return *this;
}


template<class T>
qxm::list<T>::~list()
{
	clear();
	delete _head;
	_head = nullptr;
}

template<class T>
typename qxm::list<T>::iterator qxm::list<T>::begin()
{
	return iterator(_head->_next);
}

template<class T>
typename qxm::list<T>::iterator qxm::list<T>::end()
{
	return iterator(_head);
}

template<class T>
typename qxm::list<T>::const_iterator qxm::list<T>::begin()const
{
	return const_iterator(_head->_next);
}

template<class T>
typename qxm::list<T>::const_iterator qxm::list<T>::end()const
{
	return const_iterator(_head);
}

template<class T>
void qxm::list<T>::push_back(const T& x)
{
	//node* tail = _head->_prev; //记录插入数据前的尾部结点
	//node* newnode = new node(x);

	//tail->_next = newnode;//指针改变的顺序不影响结果
	//newnode->_prev = tail;
	//newnode->_next = _head;
	//_head->_prev = newnode;
	insert(end(), x);
}

template<class T>
void qxm::list<T>::insert(iterator pos, const T x)
{
	node* cur = pos._node;
	node* prev = cur->_prev;
	node* newnode = new node(x);

	prev->_next = newnode;
	cur->_prev = newnode;
	newnode->_prev = prev;
	newnode->_next = cur;
}

template<class T>
void qxm::list<T>::push_front(const T& x)
{
	insert(begin(), x);
}

template<class T>
typename qxm::list<T>::iterator qxm::list<T>::erase(iterator pos)
{
	assert(pos != end());

	node* prev = pos._node->_prev;
	node* next = pos._node->_next;

	prev->_next = next;
	next->_prev = prev;
	delete pos._node;

	return iterator(next);
}

template<class T>
void qxm::list<T>::pop_back()
{
	erase(--end());
}

template<class T>
void qxm::list<T>::pop_front()
{
	erase(begin());
}

template<class T>
void qxm::list<T>::clear()
{
	iterator it = begin();
	while (it != end())
	{
		//it = erase(it);
		erase(it++);
	}
}


//////////////test-func/////////////////
template<class T>
void print_list(const qxm::list<T>& l)
{
	//l._head = nullptr; -- error,因为const修饰的是list类型，list类型内的_head不能被修改
	//l._head->_next->_data = 66; --因为const修饰的是list类型，但不修饰_head内的数据
	for (auto e : l)
	{
		e = 66; // -- e是传值返回所得，修改的不是结点内的数据
		cout << e << ' ';
	}
	cout << endl;

	typename qxm::list<T>::const_iterator it = l.begin();
	while (it != l.end())
	{
		cout << *it << endl;
		it++;
	}
	cout << endl;

}

void test_list1()
{
	qxm::list<int> l;
	l.push_back(1);
	l.push_back(2);
	l.push_back(3);

	print_list(l);

	//qxm::list<int>::iterator it = l.begin();

	//qxm::list<int>::iterator it = l.end();
	//it--;
	//while (it != l.end())
	//{
	//	//(*it) = 2;

	//	cout << *it << " ";
	//	--it;
	//}
	//cout << endl;
	//for (auto e : l)
	//{
	//	cout << e << ' ';
	//}
}

struct AA
{
	AA(int a1 = 1, int a2 = 2)
		:_a1(a1),
		_a2(a2)
	{}
	int _a1;
	int _a2;
};

void test_list2()
{
	qxm::list<AA> l;
	l.push_back(AA(1, 1));
	l.push_back(AA(2, 2));
	l.push_back(AA(3, 3));

	for (qxm::list<AA>::iterator it = l.begin(); it != l.end(); it++)
	{
		cout << it->_a1 << ":" << it->_a2 << endl;
	}
}

void test_list3()
{
	qxm::list<int> l;

	l.push_back(1);
	l.push_back(2);
	l.push_back(3);
	for (auto e : l)
	{
		cout << e << ' ';
	}
	cout << endl;

	auto pos = l.begin();
	pos++;
	l.insert(pos, 20);
	l.push_front(66);
	for (auto e : l)
	{
		cout << e << ' ';
	}
	cout << endl;
	/*std::list<int>::const_iterator it = l.begin();

	l.erase(it);*/
	/*l.clear();*/

	for (auto e : l)
	{
		cout << e << ' ';
	}
	cout << endl;
	l.push_back(1);
	l.push_back(2);
	l.push_back(3);
	//l.pop_back();
	//l.pop_front();
	qxm::list<int> l1(l);
	l1.push_back(99);
	for (auto e : l)
	{
		cout << e << ' ';
	}
	cout << endl;

	for (auto e : l1)
	{
		cout << e << ' ';
	}
	cout << endl;
	l1 = l;
	for (auto e : l)
	{
		cout << e << ' ';
	}
	cout << endl;

	for (auto e : l1)
	{
		cout << e << ' ';
	}
	cout << endl;
}