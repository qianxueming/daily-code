#include "Stack.h"


int main()
{
	ST st;
	StackInit(&st);
	StackPush(&st, 1);
	StackPush(&st, 2);
	StackPush(&st, 3);
	StackPush(&st, 4);
	StackPush(&st, 5);

	/*StackPop(&st);
	StackPop(&st);
	StackPop(&st);
	StackPop(&st);
	StackPop(&st);
	StackPop(&st);*/

	printf("%d\n", StackTop(&st));
	printf("size:%d\n", StackSize(&st));
	StackDestroy(&st);
	return 0;
}