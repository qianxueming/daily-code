//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	int b = 0;
//	int& a = b; //引用定义时必须初始化，并且指向不会在改变
//	cout << &b << endl;
//	cout << &a << endl;
//
//	int c = 1;
//	a = c; //此处为赋值操作，并没有改变a的指向
//	cout << &c << endl;
//	cout << &a << endl;
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//void Swap(int& a, int& b)
//{
//	int temp = a;
//	a = b;
//	b = temp;
//}
//
//int main()
//{
//	int a = 1;
//	int b = 0;
//	Swap(a, b);
//	cout << a << endl;
//	cout << b << endl;
//	return 0;
//}


//#include <iostream>
//
//using namespace std;
//
//int& Count()
//{
//	static int n = 0;
//	n++;
//
//	return n;
//}
//
//int main()
//{
//	int ret = Count();
//	cout << ret << endl;
//	return 0;
//}
//
//#include <iostream>
//#include <assert.h>
//
//using namespace std;
//
//#define N 10
//typedef struct ARRAY
//{
//	int a[N];
//	int size;
//}AY;
//
//int& PosAt(AY& ay, int i)
//{
//	assert(i < N);
//
//	return ay.a[i];
//}
//
//int main()
//{
//	AY ay;
//	for (int i = 0; i < 10; i++)
//	{
//		PosAt(ay, i) = i * 10;
//	}
//
//	for (int i = 0; i < 10; i++)
//	{
//		cout << PosAt(ay, i) << endl;
//	}
//	return 0;
//}


//#include <iostream>
//
//using namespace std;

//int& Add(int a, int b)
//{
//	int c = a + b;
//
//	return c;
//}
//
//int main()
//{
//	int& ret = Add(1, 2);
//
//	cout << "Add(1, 2) is :" << ret << endl;
//	cout << "Add(1, 2) is :" << ret << endl;
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//#include <time.h>
//struct A { int a[10000]; };
//A a;
//// 值返回
//A TestFunc1() { return a; }
//// 引用返回
//A& TestFunc2() { return a; }
//void TestReturnByRefOrValue()
//{
//	// 以值作为函数的返回值类型
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc1();
//	size_t end1 = clock();
//	// 以引用作为函数的返回值类型
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc2();
//	size_t end2 = clock();
//	// 计算两个函数运算完成之后的时间
//	cout << "TestFunc1 time:" << end1 - begin1 << endl;
//	cout << "TestFunc2 time:" << end2 - begin2 << endl;
//}
//
//int main()
//{
//	TestReturnByRefOrValue();
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//#include <time.h>
//struct A { int a[10000]; };
//void TestFunc1(A a) {}
//void TestFunc2(A& a) {}
//void TestRefAndValue()
//{
//	A a;
//	// 以值作为函数参数
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc1(a);
//	size_t end1 = clock();
//	// 以引用作为函数参数
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc2(a);
//	size_t end2 = clock();
//	// 分别计算两个函数运行结束后的时间
//	cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
//	cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
//}
//
//int main()
//{
//	TestRefAndValue();
//	return 0;
//}


//#include <iostream>
//
//using namespace std;
//
//int Count()
//{
//	static int n = 0;
//	n++;
//
//	return n;
//}
//
//int main()
//{
//	//指针和引用，赋值/初始化 权限可以缩小，但是不能放大
//
//	//权限放大
//	//const int a = 1;
//	//int& b = a;//a为只读的，引用成可读可写的，权限放大
//
//	//const int* p1 = NULL;
//	//int* p2 = p1;//p1的指向为只读的，引用成可读可写的，权限放大
//
//	//权限保持
//	const int c = 1;
//	const int d = c;//c为只读，引用成只读的，权限保持
//
//	const int* p3 = NULL;
//	const int* p4 = p3;//p3的指向为只读，引用成只读的，权限保持
//
//	//权限缩小
//	int x = 1;
//	const int& y = x;//x为可读可写，引用成只读的，权限缩小
//
//	int* p5 = NULL;
//	const int* p6 = p5;//p5的指向为可读可写，引用成只读的，权限缩小
//
//	const int m = 1;
//	int n = m;//非指针，非引用，n的改变不影响m，因此将m的值赋值给n不影响
//
//	//int& ret = Count();//函数返回临时变量，临时变量具有常性，无法修改，因此为权限放大
//	const int& ret = Count();
//
//	int i = 0;
//	//double& rd = i;//类型转换，会产生临时变量，先产生临时变量，再将i的值拷贝给临时变量，然后将临时变量引用，临时变量具有常性
//	const double& rd = i;
//	return 0;
//}


//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//	int a = 0;
//
//	int& ra = a;
//	ra = 20;
//
//	int* pa = &a;
//	*pa = 20;
//
//	return 0;
//}

//#include <iostream>
//
//using namespace std;
//
//int Testauto()
//{
//	return 10;
//}
//
//int main()
//{
//	int a = 0;
//	auto b = a;
//	auto c = &a;
//	auto d = Testauto();
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(d).name() << endl;
//
//
//
//	return 0;
//}
//
//#include <iostream>
//using namespace std;
//#include <string>
//#include <map>

//int main()
//{
//	std::map<std::string, std::string> m{ { "apple", "苹果" }, { "orange",
//   "橙子" },
//	  {"pear","梨"} };
//	//std::map<std::string, std::string>::iterator it = m.begin();
//	auto it = m.begin();
//	cout << typeid(it).name() << endl;
//	return 0;
//}

//#include <iostream>

//typedef char* pstring;
//int main()
//{
//	const pstring p1;    // 编译成功还是失败？
//	const pstring* p2;   // 编译成功还是失败？
//	return 0;
//}

#include <iostream>

using namespace std;

struct A {

	long a1;

	short a2;

	int a3;

	int* a4;

};

void f(int)
{
	cout << "f(int)" << endl;
}
void f(int*)
{
	cout << "f(int*)" << endl;
}
int main()
{
	f(0);
	f(NULL);
	f((int*)NULL);
	cout << sizeof(struct A) << endl;
	return 0;
}

//int main()
//{
//	int a = 0;
//	auto* c = &a;
//
//	cout << typeid(c).name() << endl;
//
//
//	return 0;
//}