#include <iostream>
#include <assert.h>

using namespace std;

class Date
{
public:
	Date(int year = 1, int month = 1, int day = 1);



	void Print();

	~Date() {}//析构函数

	int GetMonthDay(int year, int month);

	Date GetAfterDay(int x);

	bool operator==(const Date& d);//==运算符重载

	bool operator<(const Date& d);// < 运算符重载

	bool operator<=(const Date& d);// <= 运算符重载

	bool operator>(const Date& d);// > 运算符重载

	bool operator>=(const Date& d);// >= 运算符重载

	//Date& operator=(const Date& d);// = 运算符重载

private:
	int _year;
	int _month;
	int _day;
};

Date::Date(int year, int month, int day)//构造函数
{
	_year = year;
	_month = month;
	_day = day;
}

void Date::Print()
{
	cout << _year << "年" << _month << "月" << _day << "日" << endl;
}

//Date::Date(const Date& d)//拷贝构造函数
//{
//	_year = d._year;
//	_month = d._month;
//	_day = d._day;
//}

int Date::GetMonthDay(int year, int month)//获得该月份天数
{
	assert(month > 0 && month < 13);

	int monthArray[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
	{
		return 29;
	}
	else
	{
		return monthArray[month];
	}
}


Date Date::GetAfterDay(int x)//获得x天后的日期(不能传入负数)
{
	Date tmp = *this;//拷贝原数据

	tmp._day += x;//将x天加到日期上
	while (tmp._day > GetMonthDay(tmp._year ,tmp._month))//如果加上x天后日期超过该月份最大天数，进行月进位
	{
		tmp._day -= GetMonthDay(tmp._year, tmp._month);//月份进位
		tmp._month++;

		if (tmp._month == 13)//月份超过最大月份，年份进位
		{
			tmp._year++;
			tmp._month = 1;
		}
	}

	return tmp;
}

bool Date::operator==(const Date& d)//==运算符重载
{
	return _year == d._year
		&& _month == d._month
		&& _day == d._day;
}

bool Date::operator<(const Date& d)// < 运算符重载
{
	if (_year < d._year)
	{
		return true;
	}
	else if (_year == d._year && _month < d._month)
	{
		return true;
	}
	else if (_year == d._year && _month && d._month && _day < d._day)
	{
		return true;
	}
	else
	{
	    return false;
	}
}

bool Date::operator<=(const Date& d)// <= 运算符重载
{
	return *this < d || *this == d;
}

bool Date::operator>(const Date& d)// > 运算符重载
{
	return !(*this <= d);
}

bool Date::operator>=(const Date& d)// >= 运算符重载
{
	return !(*this < d);
}

//Date& Date::operator=(const Date& d)// = 运算符重载
//{
//	if (this != &d)
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//
//	return *this;
//}

//int main()
//{
//	Date d1(2023, 3, 4);
//	d1.Print();
//	Date d2 = d1.GetAfterDay(150);
//	d2.Print();
//	return 0;
//}

//int main()
//{
//	Date d1(2023, 3, 4);
//	Date d2(2023, 3, 5);
//	Date d3(2023, 8, 8);
//
//	//operator==(d1, d2)
//		//d1 == d2;
//	/*cout << operator==(d1, d2) << endl;
//	cout << (d1 == d2) << endl;*/
//
//	//cout << d1.operator==(d2) << endl;
//	//cout << (d1 == d2) << endl;
//
//	/*cout << (d1 == d2) << endl;
//	cout << (d1 < d2) << endl;
//	cout << (d1 <= d2) << endl;
//	cout << (d1 > d2) << endl;
//	cout << (d1 >= d2) << endl;*/
//
//
//	d1 = d2 = d3;
//
//	d1.Print();
//	d2.Print();
//	d3.Print();
//
//	return 0;
//}
//
class Stack
{
public:
	Stack(int n = 4)//构造函数
	{
		_a = (int*)malloc(sizeof(int) * n);
		if (_a == nullptr)
		{
			perror("malloc fail");
			exit(-1);
		}
		_size = 0;
		_capacity = n;
	}
	~Stack()//析构函数
	{
		cout << "~Stack()" << endl;
		free(_a);
		_a = nullptr;
		_size = 0;
		_capacity = 0;
	}
private:
	int* _a;
	int _size;
	int _capacity;
};
//
//class Queue
//{
//
//	//使用默认生成的构造函数
//
//private:
//	Stack popst;
//	Stack pushst;
//};
//
//
//int main()
//{
//	Queue q1;
//	Queue q2;
//	 
//	q1 = q2;
//	return 0;
//}

//int main()
//{
//	Date d1(2023, 3, 4);
//	Date d2(d1);
//
//	d1.Print();
//	d2.Print();
//
//	return 0;
//}

int main()
{
	Stack st1;
	Stack st2;
	st2 = st1;

	return 0;
}